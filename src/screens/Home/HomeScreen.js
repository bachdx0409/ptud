import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import {SwiperFlatList} from 'react-native-swiper-flatlist';
import {ListBanner} from '../../constants/ListBanner';
import UserScreen from '../Account/UserScreen';
import {useNavigation} from '@react-navigation/native';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import storage from '@react-native-firebase/storage';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {ScrollView} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';

const {width, height} = Dimensions.get('window');
const imageWidth = width - 20;

const data = [
  {
    img: require('../../assets/image/venom2.jpeg'),
    name: 'VENOM 2',
    content:
      'Venom 2: Đối mặt tử thù - Venom: Let There Be Carnage là bộ phim siêu anh hùng của Mỹ được ra mắt vào năm 2021. Bộ phim do Columbia Pictures cùng với Marvel và Tencent Pictures đồng sản xuất. Bộ phim được đạo diễn bởi Andy Serkis, từ phần kịch bản của Kelly Marcel dựa trên cốt truyện cô viết cùng Hardy. Phim có sự tham gia của các diễn viên Tom Hardy, Michelle Williams, Naomie Harris, Reid Scott, Stephen Graham và Woody Harrelson.',
  },
  {
    img: require('../../assets/image/endGame.png'),
    name: 'endGame',
    content:
      'Biệt Đội Siêu Anh Hùng 4: Tàn Cuộc (Avengers 4: Endgame) ra mắt vào tháng 4/2019 sẽ giải quyết triệt để những khúc mắc đã vạch ra suốt 22 bộ phim trước đó của Vụ trụ điện ảnh Marvel (MCU). Hai tháng sau đó, Người Nhện 2 là khởi đầu hoàn toàn mới dành cho MCU. Trong phần trước, tên Titan điên rồ luôn bị ám ảnh về sứ mệnh phải "cân bằng" vũ trụ. Sau khi thu thập đủ 6 viên đá vô cực, Thanos khiến nửa thế giới tan biến thành cát bụi chỉ bằng một cái búng tay',
  },
  {
    img: require('../../assets/image/fastFurious9.jpeg'),
    name: 'fastFurious9',
    content:
      'Quá Nhanh Quá Nguy Hiểm 9 - Fast & Furious 9 nối tiếp những câu chuyện từ phần trước, Dom Toretto hiện đang có một cuộc sống bình yên với người vợ Letty Ortiz và câu con trai Brian. Thế nhưng, bỗng chốc cuộc sống của anh đảo lộn khi một người đàn ông bí ẩn xuất hiện và khiêu khích anh, có gây hại đến những người thân yêu của Dom. Lần này Dom sẽ phải đối mặt với mối đe dọa và những tội lỗi trong quá khứ bắt đầu ám ảnh lấy anh. Dom cùng với những người bạn của mình chiến đấu để bảo vệ người thân yêu của mình.',
  },
  {
    img: require('../../assets/image/kingsman.jpeg'),
    name: 'kingsman',
    content:
      ' The Secret Service là một bộ phim hành động điệp viên của Anh năm 2015 do Matthew Vaughn đạo diễn, dựa trên cuốn truyện tranh The Secret Service do Dave Gibbons và Mark Millar tạo ra. Kịch bản phim do Vaughn và Jane Goldman viết. Phim xoay quanh câu chuyện đào tạo và tuyển dụng của một điệp viên bí mật tiềm năng Gary "Eggsy" Unwin (Taron Egerton) vào một tổ chức điệp viên bí mật',
  },
];

const HeaderHome = () => {
  return (
    <View style={styles.header}>
      <SwiperFlatList
        style={{width: imageWidth, borderRadius: 10}}
        autoplay
        autoplayLoop
        autoplayDelay={3}
        autoplayLoopKeepAnimation
        index={0}
        showPagination={true}
        paginationStyleItem={{width: 5, height: 5}}
        data={ListBanner}
        renderItem={({item, index}) => (
          <View
            key={index}
            style={{
              width,
              height: 200,
            }}>
            <Image
              source={item}
              style={{width: '100%', alignSelf: 'center', height: 200}}
            />
          </View>
        )}
      />
    </View>
  );
};

const HomeScreen = ({navigation}) => {
  const refCarousel = useRef(null);
  const [listFilmTopick, setListFilmTopick] = useState(data);
  const [index, setIndex] = useState(0);
  const user = useSelector(state => state.slice.user);

  const on = () => {
    launchImageLibrary({}, async response => {
      
      if (response.didCancel) {
      } else if (response.error) {
        alert('Có lỗi xảy ra !', error);
      } else {
        const timeStamp = new Date();
        // const source = {uri: response.assets[0].uri};
        const source = response.assets[0].uri; //thu vien
        const reference = storage().ref(`${timeStamp}.png`);
        await reference.putFile(source);
        const url = await storage().ref(`${timeStamp}.png`).getDownloadURL();
        
        // setUrl(url);
      }
    });
  };

  useEffect(() => {
    if (index == [...listFilmTopick].length - 1) {
      setTimeout(() => {
        refCarousel.current?.snapToItem(0);
      }, 2000);
    }
  }, [index]);

  const renderFilmTopick = ({item}, parallaxProps) => {
    return (
      <TouchableOpacity onPress={on}>
        <Image style={styles.imageTopick} source={item.img} />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <SafeAreaView />
      <ScrollView style={{flex: 1}}>
        <View style={styles.top}>
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Image
              style={styles.imgOption}
              source={require('../../assets/icons/icOption.png')}
            />
          </TouchableOpacity>
          <Image
            style={styles.imgLogo}
            source={require('../../assets/icons/logoApp.png')}
          />
          <TouchableOpacity onPress={() => navigation.navigate('User')}>
            <Image style={styles.imgUser} source={{uri: user.image}} />
          </TouchableOpacity>
        </View>
        <HeaderHome />
        <View style={styles.listFilmTopick}>
          <Carousel
            onLayout={a => console.log(a)}
            ref={refCarousel}
            data={listFilmTopick}
            renderItem={renderFilmTopick}
            sliderWidth={width}
            itemWidth={width * 0.75}
            layout={'default'}
            autoplay
            onBeforeSnapToItem={i => setIndex(i)}
          />
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: 'rgba(255,255,255,0.7)',
            alignSelf: 'center',
            marginTop: 10,
            borderRadius: 15,
          }}>
          <Text
            style={{
              marginTop: 15,
              marginLeft: '5%',
              fontSize: 18,
              fontWeight: '500',
            }}>
            Phim: {listFilmTopick[index]?.name}
          </Text>
          <Text
            style={{
              marginHorizontal: '5%',
              marginTop: 10,
              fontSize: 16,
              fontWeight: '400',
            }}>
            {listFilmTopick[index]?.content}
          </Text>
        </View>
        <View style={{width: '100%', height: 70}}></View>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(206,90,70,1)',
  },
  header: {
    width: width,
    height: 200,
    alignItems: 'center',
  },
  wrapper: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  top: {
    width: width,
    height: 80,
    flexDirection: 'row',
    //backgroundColor:'lightblue',
    justifyContent: 'space-around',
  },
  imgUser: {
    width: 30,
    height: 30,
    marginTop: 10,
    borderRadius: 50,
  },
  imgLogo: {
    width: 200,
    height: 60,
    marginTop: 5,
  },
  imgOption: {
    width: 25,
    height: 25,
    marginTop: 10,
  },
  listFilmTopick: {
    marginTop: 20,
  },
  imageTopick: {
    width: '88%',
    height: width * 0.6 * 1.5,
    resizeMode: 'stretch',
    alignSelf: 'center',
    borderRadius: 5,
  },
  imageContainer: {
    width: '100%',
    height: 250,
  },
});
