import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  TouchableNativeFeedback,
  Dimensions,
  ImageBackground,
  TouchableWithoutFeedback,
  Alert,
  Modal,
  TextInput,
  Image,
} from 'react-native';
import Header from '../../common/Header';
import {Picker} from '@react-native-picker/picker';
import {launchImageLibrary} from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';

const ManaFilm = () => {
  const [showAdd, setShowAdd] = useState<boolean>(false);
  const [name, setName] = useState<String | any>('');
  const [description, setDescription] = useState<String | any>('');
  const [type, setType] = useState('Hành động / Viễn tưởng');
  const [image, setImage] = useState<any>();
  const [time, setTime] = useState<any>();
  const [data, setData] = useState([]);
  const [nameData, setnameData] = useState([]);
  const [dateData, setDateData] = useState([]);
  const [desData, setDesData] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  console.log('image', image);

  const selectImage = () => {
    launchImageLibrary({}, response => {
      if (response.didCancel) {
      } else if (response.error) {
        Alert.alert('Lỗi', error);
      } else {
        setImage(response.assets[0]);
      }
    });
  };

  const validate = () => {
    if (name == undefined || name == '') {
      return {
        type: true,
        mess: 'Nhập tên',
      };
    } else if (description == undefined || description == '') {
      return {
        type: true,
        mess: 'Nhập mô tả',
      };
    } else if (image == undefined || image == null) {
      return {
        type: true,
        mess: 'Chọn ảnh',
      };
    } else if (type == undefined || type == null) {
      return {
        type: true,
        mess: 'Chọn thể loại phim',
      };
    } else if (time == undefined || time == null) {
      return {
        type: true,
        mess: 'Nhập thời lượng',
      };
    }
    return {
      type: false,
    };
  };
  console.log('type', type);
  function timeConvert(n) {
    const num = Number(n);
    const hours = num / 60;
    const rhours = `0${Math.floor(hours)}`.slice(-2);
    const minutes = (hours - Math.floor(hours)) * 60;
    const rminutes = `0${Math.round(minutes)}`.slice(-2);
    return rhours + ':' + rminutes;
  }

  const addFilm = async () => {
    if (validate().type) {
      Alert.alert('', validate().mess);
      return;
    }
    const timeHour = timeConvert(time);
    const timeStamp = new Date();
    const reference = storage().ref(`${timeStamp}.png`);
    await reference.putFile(image.uri);
    const url = await storage().ref(`${timeStamp}.png`).getDownloadURL();
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    const raw = JSON.stringify({
      name: name,
      type: type,
      date: timeHour,
      image: url,
      description: description,
    });

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch('http://PTUDHTCinema.somee.com/api/createFilm', requestOptions)
      .then(response => response.text())
      .then(result => {
        console.log('result', result);
        Alert.alert('', `${result?.message}`, [
          {
            onPress: () => getData(),
          },
        ]);
      })
      .catch(error => console.log('error', error));
  };
  console.log('data', data);
  const getData = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch('http://PTUDHTCinema.somee.com/api/getFilm', requestOptions)
      .then(response => response.json())
      .then(result => setData(result?.data))
      .catch(error => console.log('error', error));
  };

  useEffect(() => {
    getData();
  }, []);

  const deleteFilm = (id: any) => {
    const requestOptions = {
      method: 'DELETE',
      redirect: 'follow',
    };

    fetch(
      `http://PTUDHTCinema.somee.com/api/deleteFilm?id=${id}`,
      requestOptions,
    )
      .then(response => response.json())
      .then(result => {
        console.log(result);
        getData();
      })
      .catch(error => console.log('error', error));
  };

  const onPressItem = (id: number) => {
    Alert.alert('', 'Bạn có muốn xoá phim', [
      {
        text: 'Trở lại',
      },
      {
        text: 'Xoá',
        onPress: () => deleteFilm(id),
      },
    ]);
  };

  const renderItem = ({item, index}: any) => {
    const id = item?.id;
    return (
      <TouchableWithoutFeedback
        onLongPress={() => onPressItem(id)}
        onPress={() => {
          setModalVisible(!modalVisible),
            setnameData(item.name),
            setDateData(item.date),
            setDesData(item.description);
        }}>
        {/* =======
       onLongPress={() => onPressItem(id)}
       onPress={()=>console.log('data bách',item) }
       >
>>>>>>> 449a9696ec42598317627a76e86636b8e52f1f4e */}
        <ImageBackground
          style={styles.item}
          imageStyle={{borderRadius: 10}}
          source={{uri: item.image}}></ImageBackground>
      </TouchableWithoutFeedback>
    );
  };

  const CustomModal = () => {
    return (
      <Modal visible={modalVisible} transparent={true} animationType="none">
        <View style={stylesModal.centeredView}>
          <View style={stylesModal.modalView}>
            <View style={stylesModal.wrppModal}>
              <Text style={{fontSize: 18, fontWeight: '600'}}>
                Thông tin phim
              </Text>
              <View style={stylesModal.boxTxt}>
                <Text style={stylesModal.txt}>Tên phim: {nameData}</Text>
                <Text style={stylesModal.txt}>Thời gian: {dateData}</Text>
                <Text style={stylesModal.txt}>Thông tin: {desData}</Text>
              </View>
              <TouchableOpacity
                onPress={() => setModalVisible(!modalVisible)}
                style={stylesModal.cancel}>
                <Text style={stylesModal.btnCancel}>Thoát</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  return (
    <View style={styles.container}>
      <SafeAreaView />
      <CustomModal />
      <Header title="Danh sách phim" />
      <TouchableWithoutFeedback onPress={() => setShowAdd(true)}>
        <View style={styles.btAdd}>
          <Text style={styles.txtAdd}>+ Thêm</Text>
        </View>
      </TouchableWithoutFeedback>

      <FlatList
        data={data}
        renderItem={renderItem}
        style={styles.flatListContainer}
        numColumns={2}
        columnWrapperStyle={{justifyContent: 'space-between'}}
      />

      <Modal transparent={true} visible={showAdd}>
        <TouchableWithoutFeedback onPress={() => setShowAdd(false)}>
          <View style={styles.modalContainer}>
            <TouchableWithoutFeedback>
              <View style={styles.modal}>
                <Text style={styles.title}>Thêm phim mới</Text>
                <Text style={styles.txtType}>Tên phim:</Text>
                <TextInput
                  style={styles.input}
                  value={name}
                  onChangeText={setName}
                />
                <Text style={styles.txtType}>Thể loại :</Text>
                <Picker
                  selectedValue={type}
                  onValueChange={(itemValue, itemIndex) => setType(itemValue)}>
                  <Picker.Item
                    label="Hành động / Viễn tưởng"
                    value="Hành động / Viễn tưởng"
                  />
                  <Picker.Item label="Kinh dị" value="Kinh dị" />
                  <Picker.Item label="Tình cảm" value="Tình cảm" />
                  <Picker.Item label="Thám hiểm" value="Thám hiểm" />
                  <Picker.Item label="Hoạt hình" value="Hoạt hình" />
                  <Picker.Item label="Trinh thám" value="Trinh thám" />
                </Picker>
                <Text style={styles.txtType}>Mô tả :</Text>
                <TextInput
                  style={{...styles.input, height: 70}}
                  value={description}
                  multiline
                  onChangeText={setDescription}
                />
                <Text style={styles.txtType}>Thời lượng : (phút)</Text>
                <TextInput
                  style={{...styles.input}}
                  value={time}
                  onChangeText={setTime}
                  keyboardType="numeric"
                />
                <Text style={styles.txtType}>Ảnh đính kèm :</Text>
                <TouchableWithoutFeedback onPress={selectImage}>
                  <View style={styles.boxAddImage}>
                    {image ? (
                      <Image style={{flex: 1}} source={{uri: image.uri}} />
                    ) : (
                      <Text style={styles.icon}>+</Text>
                    )}
                  </View>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={addFilm}>
                  <View style={styles.btConfirm}>
                    <Text style={styles.txtBtConfirm}>Thêm</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
};

export default ManaFilm;

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  btAdd: {
    width: 100,
    height: 30,
    backgroundColor: '#79cd00',
    borderRadius: 10,
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginRight: '5%',
  },
  txtAdd: {
    fontSize: 18,
    fontWeight: '500',
    color: '#fff',
    alignSelf: 'center',
  },
  flatListContainer: {
    width: '90%',
    height: '100%',
    alignSelf: 'center',
    marginTop: 10,
  },
  item: {
    width: width * 0.9 * 0.48,
    height: 120,
    marginTop: 10,
    alignSelf: 'center',
    borderRadius: 10,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
    justifyContent: 'center',
  },
  modal: {
    width: '94%',
    height: '80%',
    alignSelf: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
    paddingHorizontal: '5%',
  },
  title: {
    alignSelf: 'center',
    marginTop: 10,
    fontSize: 20,
    fontWeight: '500',
  },
  txtType: {
    fontSize: 17,
    fontWeight: '400',
    marginTop: 10,
  },
  input: {
    width: '100%',
    height: 35,
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    fontSize: 15,
  },
  boxAddImage: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    borderRadius: 5,
    borderWidth: 1,
    marginTop: 10,
    justifyContent: 'center',
  },
  icon: {
    alignSelf: 'center',
    fontSize: 30,
  },
  btConfirm: {
    width: 150,
    height: 40,
    alignSelf: 'center',
    backgroundColor: '#79cd00',
    justifyContent: 'center',
    marginTop: 50,
    borderRadius: 10,
  },
  txtBtConfirm: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 18,
  },
});

const stylesModal = StyleSheet.create({
  centeredView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(0,0,0,0.6)'
  },
  modalView: {
    margin: 50,
    backgroundColor: '#FFF',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  wrppModal: {
    width: width * 0.6,
    height: height * 0.4,
    alignItems: 'center',
  },
  cancel: {
    width: width * 0.5,
    height: height * 0.05,
    marginTop: 5,
    borderWidth: 1,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
    borderColor: '#47c9dede',
  },
  btnCancel: {
    fontSize: 15,
    fontWeight: '600',
    color: '#47c9dede',
  },
  boxTxt: {
    marginTop: 20,
    width: '100%',
    height: height * 0.28,
  },
  txt: {
    fontSize: 15,
    fontWeight: '600',
    paddingBottom: 5,
  },
});
