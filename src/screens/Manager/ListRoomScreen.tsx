import React from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {View, Text} from 'react-native';
import Header from '../../common/Header';

interface propsItem {
  item: any;
  index: number;
}

const ListRoomScreen = ({navigation}: any) => {
  const renderItem = ({item, index}: propsItem) => {
    return (
      <TouchableWithoutFeedback
        onPress={() =>
          navigation.navigate('ManaRoom', {
            name: index+1,
          })
        }>
        <View style={styles.item}>
          <Text style={styles.txtRoom}>Phòng số {index + 1}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <View style={styles.container}>
      <SafeAreaView />
      <Header title={'Danh sách các phòng'} />
      <FlatList
        data={[1, 2, 3]}
        renderItem={renderItem}
        ListFooterComponent={() => <View style={{width: '100%', height: 50}} />}
      />
    </View>
  );
};

export default ListRoomScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  item: {
    width: '90%',
    alignSelf: 'center',
    height: 70,
    marginTop: 15,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: 'center',
    backgroundColor: 'rgba(206,90,70,1)',
  },
  txtRoom: {
    fontSize: 18,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#fff',
  },
});
