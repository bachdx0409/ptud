import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableNativeFeedback,
  Image,
  TouchableOpacity,
  Modal,
  Alert,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
} from 'react-native';
import Header from '../../common/Header';
import {useRoute} from '@react-navigation/native';
import CustomInput from '../../common/CustomInput';
import API from '../../constants/API';
import ListTime from '../../constants/ListTime';

type typeItem = {
  name: string;
  type: string;
  date: string;
  image: string;
  description: string;
  dateIn: string;
  dateOut: string;
};
interface propsItem {
  item: typeItem;
  index: number;
}

const ManaRoom = () => {
  const route = useRoute() as any;
  const [data, setData] = useState<any>([]);
  const [modalAdd, setModalAdd] = useState(false);
  const [film, setFilm] = useState<any>('');
  const [time, setTime] = useState('');
  const [listFilm, setListFilm] = useState([]);
  const [modalSelectFilm, setModalSelectFilm] = useState<boolean>(false);
  const [modalSelectTime, setModalSelectTime] = useState<boolean>(false);

  const getData = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(`${API}/api/getFilm`, requestOptions)
      .then(response => response.json())
      .then(result => {
        setListFilm(result.data);
      })
      .catch(error => console.log('error', error));

    fetch(`${API}/api/getSchedle?RoomId=${route?.params?.name}`, requestOptions)
      .then(response => response.json())
      .then(result => {
        setData(result.data);
      })
      .catch(error => console.log('error', error));
  };

  useEffect(() => {
    getData();
  }, []);

  const onPressItem = (index: number) => {
    Alert.alert('', 'Bạn có muốn xoá phim', [
      {
        text: 'Trở lại',
      },
      {
        text: 'Xoá',
        onPress: () => setData(data.filter((a: any, b: number) => a != index)),
      },
    ]);
  };

  const onPressCancel = () => {
    setModalAdd(false);
    setFilm('');
  };

  const onPressFIlm = (item: any) => {
    setFilm(item);
    setModalSelectFilm(false);
  };

  const onPressTime = (item: any) => {
    setTime(item.time);
    setModalSelectTime(false);
  };

  const submit = async () => {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    const date = new Date();
    const dataString = JSON.stringify(date).slice(1, 25);
    const timeDate = dataString.split('T');
    console.log('timeDate', dataString);
    const dateIn = timeDate[0] + ' ' + time + ':00';
    const time1 = time.split(':');
    const time2 = film.date.split(':');
    let hour = '';
    let minuter = '';
    let sumMinuter = (Number(time1[1]) + Number(time2[1])) as any;
    if (sumMinuter >= 60) {
      sumMinuter = `00${sumMinuter - 60}`;
      minuter = sumMinuter.slice(-2);
      let sumHour = (Number(time1[0]) + Number(time2[0]) + 1) as any;
      sumHour = `00${sumHour}`;
      hour = sumHour.slice(-2);
    } else {
      sumMinuter = `00${sumMinuter}`;
      minuter = sumMinuter.slice(-2);
      let sumHour = (Number(time1[0]) + Number(time2[0])) as any;
      sumHour = `00${sumHour}`;
      hour = sumHour.slice(-2);
    }
    const dateOut = `${timeDate[0]} ${hour}:${minuter}:00`;
    const raw = JSON.stringify({
      filmId: film.id,
      roomId: route?.params?.name,
      name: film.name,
      price: '100000',
      dateIn: dateIn,
      dateOut: dateOut,
      count: 45,
    });
    console.log('dateIn', raw);

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(`${API}/api/createTicket`, requestOptions as any)
      .then(response => response.json())
      .then(result => {
        Alert.alert('', result.message, [
          {
            onPress: () => getData(),
          },
        ]);
      })
      .catch(error => console.log('error', error));
  };

  const renderFilm = ({item, index}: any) => {
    return (
      <TouchableOpacity
        style={styles.rowFilm}
        onPress={() => onPressFIlm(item)}>
        <Image style={styles.avtFilm} source={{uri: item?.image}} />
        <Text style={styles.txtNameFilmItem}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  const renderTime = ({item, index}: any) => {
    return (
      <TouchableOpacity
        style={{
          ...styles.rowFilm,
          backgroundColor: 'rgba(0,0,0,0.3)',
          borderRadius: 5,
        }}
        onPress={() => onPressTime(item)}>
        <Text style={styles.txtNameFilmItem}>{item.time}</Text>
      </TouchableOpacity>
    );
  };

  const renderItem = ({item, index}: propsItem) => {
    const timeIn = item.dateIn.split('T');
    const timeOut = item.dateOut.split('T');

    return (
      <TouchableNativeFeedback onLongPress={() => onPressItem(index)}>
        <View style={styles.item}>
          <Image style={styles.imageFilm} source={{uri: item.image}} />
          <View style={styles.contentItem}>
            <Text style={styles.txtNameFilm}>{item.name}</Text>
            <View style={styles.blockTime}>
              <Text style={styles.txtNameFilm}>
                Khởi chiếu
                <Text style={[styles.txtTime, {color: 'green'}]}>
                  {' '}
                  {timeIn[1]}
                </Text>
              </Text>
              <Text style={[styles.txtNameFilm, {marginLeft: 10}]}>
                Kết thúc<Text style={styles.txtTime}> {timeOut[1]}</Text>
              </Text>
            </View>
          </View>
        </View>
      </TouchableNativeFeedback>
    );
  };

  const TabFunctions = () => {
    return (
      <View style={styles.tabFunctions}>
        <TouchableOpacity
          onPress={() => setModalAdd(true)}
          style={styles.blockFuntion}>
          <Text style={styles.txtAdd}>Thêm</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <SafeAreaView />
      <Header title={`Quản lý phòng phim ${route?.params?.name}`} />
      {TabFunctions()}
      <FlatList
        data={data}
        keyExtractor={(item: any, index: number) => index as any}
        renderItem={renderItem}
        ListFooterComponent={() => <View style={styles.footer} />}
        ListEmptyComponent={() => (
          <Text style={styles.txtEmpty}>Chưa có lịch chiếu</Text>
        )}
      />
      <Modal transparent={true} visible={modalAdd}>
        <TouchableWithoutFeedback onPress={() => setModalAdd(false)}>
          <View style={styles.containerModal}>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
              <View style={styles.viewModal}>
                <Text style={styles.txtHeader}>Thêm phim</Text>
                <CustomInput
                  title={'Tên phim'}
                  value={film?.name}
                  placeholder={'Chọn tên phim...'}
                  onPress={() => setModalSelectFilm(true)}
                />
                <CustomInput
                  title={'Thời gian chiếu'}
                  value={time}
                  placeholder={'Chọn thời gian chiếu'}
                  onPress={() => setModalSelectTime(true)}
                />
                <View style={styles.bt}>
                  <TouchableOpacity
                    onPress={onPressCancel}
                    style={styles.btCancel}>
                    <Text style={styles.txtBt}>Huỷ</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={submit}
                    style={[styles.btCancel, {backgroundColor: 'green'}]}>
                    <Text style={styles.txtBt}>Xác nhận</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
      <Modal animationType="slide" transparent={true} visible={modalSelectFilm}>
        <TouchableWithoutFeedback onPress={() => setModalSelectFilm(false)}>
          <View style={{...styles.containerModal, justifyContent: 'flex-end'}}>
            <View style={styles.bottomSheet}>
              <FlatList
                data={listFilm}
                renderItem={renderFilm}
                keyExtractor={(a, b) => JSON.stringify(b)}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
      <Modal animationType="slide" transparent={true} visible={modalSelectTime}>
        <TouchableWithoutFeedback onPress={() => setModalSelectTime(false)}>
          <View style={{...styles.containerModal, justifyContent: 'flex-end'}}>
            <View style={styles.bottomSheet}>
              <FlatList
                data={ListTime}
                renderItem={renderTime}
                keyExtractor={(a, b) => JSON.stringify(b)}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
};

export default ManaRoom;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  item: {
    width: '94%',
    height: 80,
    marginTop: 10,
    alignSelf: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    paddingHorizontal: '2%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: '#fff',
  },
  imageFilm: {
    width: 70,
    height: 70,
    borderRadius: 50,
    alignSelf: 'center',
  },
  footer: {
    width: '100%',
    height: 40,
  },
  contentItem: {
    alignSelf: 'center',
    height: 60,
    marginLeft: '1%',
  },
  txtNameFilm: {
    fontSize: 16,
    fontWeight: '500',
  },
  blockTime: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txtTime: {
    fontSize: 14,
    fontWeight: '400',
    color: 'red',
  },
  tabFunctions: {
    width: '98%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  blockFuntion: {
    width: 120,
    height: 30,
    backgroundColor: 'rgba(0,181,47,0.8)',
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  txtAdd: {
    fontWeight: '500',
    fontSize: 16,
  },
  containerModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewModal: {
    width: '90%',
    backgroundColor: '#Ffff',
    borderRadius: 5,
    paddingBottom: 30,
  },
  txtHeader: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '500',
    marginTop: 10,
  },
  bt: {
    flexDirection: 'row',
    width: '80%',
    justifyContent: 'space-around',
    alignSelf: 'center',
    marginTop: 30,
  },
  btCancel: {
    width: 100,
    height: 40,
    backgroundColor: 'red',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtBt: {
    color: '#ffff',
    fontWeight: '500',
    fontSize: 15,
  },
  txtEmpty: {
    alignSelf: 'center',
    fontSize: 15,
    marginTop: '30%',
  },
  bottomSheet: {
    width: '100%',
    height: 300,
    backgroundColor: '#fff',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  rowFilm: {
    flexDirection: 'row',
    width: '90%',
    height: 30,
    marginTop: 10,
    alignSelf: 'center',
  },
  avtFilm: {
    width: 30,
    height: 30,
    borderRadius: 30,
  },
  txtNameFilmItem: {
    marginLeft: 20,
    alignSelf: 'center',
    fontSize: 15,
    fontWeight: '400',
  },
});
