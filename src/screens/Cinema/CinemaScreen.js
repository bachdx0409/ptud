import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const CinemaScreen = () => {
    return (
        <View style={styles.container} >
            <Text style={{ alignSelf: 'center' }} >CinemaScreen</Text>
        </View>
    )
}

export default CinemaScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
})
