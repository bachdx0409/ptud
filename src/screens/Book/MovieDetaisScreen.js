import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Header from '../../common/Header';
import {useRoute} from '@react-navigation/native';
import {useNavigation} from '@react-navigation/core';

const {width, height} = Dimensions.get('window');

const MovieDetailsScreen = props => {
  const route = useRoute();
  const navigation = useNavigation();
  const data = route.params;
  console.log('route.params', route.params);
  return (
    <SafeAreaView style={styles.container}>
      <Header title={data.name} />
      <View style={styles.wrapper}>
        <Image
          source={{uri: data.image}}
          style={{width: width, height: height * 0.35}}
        />
        <View style={styles.box1}>
          <View style={styles.wrapperTitle}>
            <Text style={styles.textTitle}>{route.params.title}</Text>
            <View style={{justifyContent: 'center'}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('MovieSeat',route.params)}
                style={styles.btn}>
                <Text style={styles.textBtn}>Đặt vé</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.type}>
            <Text style={styles.textType}>{route.params.type}</Text>
          </View>
          <View style={styles.box2}>
            <Text style={styles.txtBach}>
              Thời lượng:{' '}
              <Text style={{fontSize: 12, color: 'black'}}>
                {route.params.date} giờ
              </Text>
            </Text>
          </View>
          <View style={styles.textDescription}>
            <Text style={styles.txtBach}>
              Mô tả:{' '}
              <Text numberOfLines={5} style={{fontSize: 12, color: 'black'}}>
                {route.params.description}
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    width: width,
    height: height,
  },
  box1: {
    width: width,
    height: height * 0.25,
    // borderBottomWidth: 8,
    borderColor: '#d9dada',
    alignItems: 'center',
  },
  wrapperTitle: {
    width: '100%',
    height: '30%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textTitle: {
    width: '70%',
    fontSize: 18,
    fontWeight: '600',
    alignSelf: 'center',
  },
  btn: {
    width: 80,
    height: 30,
    borderRadius: 20,
    borderWidth: 0.5,
    borderColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#d11f10',
  },
  textBtn: {
    fontWeight: '600',
    fontSize: 14,
    color: '#FFF',
  },
  type: {
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#2fa2f9',
    width: 205,
    height: 45,
    borderWidth: 1,
    borderRadius: 40,
  },
  textType: {
    color: '#60d5d5',
    fontSize: 16,
    fontWeight: '700',
  },
  textDescription: {
    width: '92%',
    marginTop: 10,
    alignSelf: 'center',
  },
  txtBach: {
    fontSize: 16,
    fontWeight: '500',
  },
  box2: {
    width: '92%',
    paddingTop: 10,
    // borderBottomWidth: 8,
    borderColor: '#d9dada',
  },
  textLeft: {
    fontSize: 13,
    color: '#555555',
    fontWeight: '600',
    textAlign: 'right',
  },
  textRight: {
    width: '76%',
    fontSize: 13,
    color: '#555555',
    fontWeight: '400',
    paddingLeft: 10,
  },
});

export default MovieDetailsScreen;
