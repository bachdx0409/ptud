import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';
import {Image} from 'react-native-animatable';
import Header from '../../common/Header';
import {useNavigation} from '@react-navigation/core';

const {width, height} = Dimensions.get('window');

const Box = props => {
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.box}>
      <Image source={{uri: props.img}} style={styles.img} />
      <View style={styles.wrapperText}>
        <View style={styles.boxTextTitle}>
          <Text style={styles.textTitle}>{props.title}</Text>
        </View>
        <View style={styles.wrapperTextTime}>
          <Text style={{color: 'gray'}}>{props.date}</Text>
          <Text style={{color: 'gray'}}>{props.time}</Text>
          <Text style={styles.type}>{props.type}</Text>
        </View>
      </View>
      <View style={styles.boxArrow}>
        <Image
          source={require('../../assets/icons/icRightArrow.png')}
          style={styles.icArrow}
        />
      </View>
    </TouchableOpacity>
  );
};

const SelectMovieScreen = () => {
  const navigation = useNavigation();
  const [show, setShow] = useState(false);
  const [room, setRoom] = useState(1);
  const [data, setData] = useState([]);
  const ListMovie = [
    {
      title: 'VENOM 2: ĐỐI MẶT TỬ THÙ',
      img: require('../../assets/image/venom2.jpeg'),
      date: '27 Thg 12, 2021',
      time: '2 giờ 15 phút',
      type: '4D',
      description: `Venom 2: Đối mặt tử thù là bộ phim tiếp nối câu chuyện ở phần 1,
      sau khi Venom sống sót và quyết định ở lại Trái Đất cùng anh chàng
      Eddie vì yêu quý hành tinh này. Nút thắt tới khi Venom chê cười
      Eddie khiến họ xô xát và chiến tranh lạnh. Đúng lúc này sát nhân
      Celtus đã hoá Venom đỏ, sát hại lính canh và thoát khỏi nhà tù.`,
      genre: 'Hành động',
      director: 'Andy Serkis',
      actor: 'Tom Hardy, Michelle Williams, Naomie Harris',
      duration: '1 giờ 37 phút',
      language: 'Tiếng Anh - Phụ đề Tiếng Việt',
    },
  ];

  const onPressFilter = () => {
    setShow(true);
  };

  const onPressRoom = a => {
    setRoom(a);
    setShow(false);
  };

  useEffect(() => {
    const requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(
      `http://PTUDHTCinema.somee.com/api/getSchedle?RoomId=${room}`,
      requestOptions,
    )
      .then(response => response.json())
      .then(result => {
        setData(result.data);
      })
      .catch(error => console.log('error', error));
  }, [room]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <Header title={`Lịch chiếu ngày hôm nay phòng ${room}`} />
        <View style={styles.wrapper}>
          <View style={styles.top}>
            <Text style={{fontSize: 16, alignSelf: 'center'}}>
              ✓ Đang chiếu
            </Text>
            <TouchableWithoutFeedback onPress={onPressFilter}>
              <Image
                style={styles.btFilter}
                source={require('../../assets/icons/filter.png')}
              />
            </TouchableWithoutFeedback>
          </View>
          {/* box1 */}
          <ScrollView>
            {data.map((item, index) => (
              <Box
                key={index}
                title={item.name}
                img={item.image}
                date={item.date}
                time={'2 giờ 15 phút'}
                type={item.type}
                onPress={() => navigation.navigate('MovieDetails', item)}
              />
            ))}
            <View style={{width: '100%', height: 60}} />
          </ScrollView>
        </View>
      </SafeAreaView>
      <Modal transparent={true} visible={show}>
        <TouchableWithoutFeedback
          onPress={() => {
            setShow(false);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.boxModal}>
              <TouchableOpacity
                onPress={() => onPressRoom(1)}
                style={styles.itemRoom}>
                <Text style={styles.txtRoom}>Phòng 1</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => onPressRoom(2)}
                style={styles.itemRoom}>
                <Text style={styles.txtRoom}>Phòng 2</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => onPressRoom(3)}
                style={styles.itemRoom}>
                <Text style={styles.txtRoom}>Phòng 3</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </>
  );
};

export default SelectMovieScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    width: width,
    height: height,
  },
  top: {
    width: width,
    height: height * 0.05,
    paddingLeft: 10,
    justifyContent: 'space-between',
    backgroundColor: '#8ED2F0',
    flexDirection: 'row',
  },
  btFilter: {
    width: 25,
    height: 25,
    alignSelf: 'center',
    marginRight: 10,
  },
  box: {
    flexDirection: 'row',
    width: width,
    height: height * 0.165,
  },
  img: {
    width: width * 0.24,
    height: height * 0.165,
  },
  wrapperText: {
    width: width * 0.67,
    height: height * 0.165,
    borderColor: '#8ED2F0',
    borderBottomWidth: 0.5,
  },
  boxTextTitle: {
    width: '100%',
    height: '50%',
    padding: 10,
    flexDirection: 'row',
  },
  textTitle: {
    fontSize: 18,
    fontWeight: '600',
    alignSelf: 'center',
  },
  wrapperTextTime: {
    paddingLeft: 10,
    width: '100%',
    height: '50%',
  },
  type: {
    color: '#60d5d5',
    fontSize: 18,
    fontWeight: '700',
    marginTop: 5,
  },
  boxArrow: {
    height: '100%',
    width: width * 0.1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#8ED2F0',
    borderBottomWidth: 0.5,
  },
  icArrow: {
    width: 25,
    height: 25,
  },
  centeredView: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.6)',
    justifyContent: 'center',
  },
  boxModal: {
    width: '90%',
    height: 230,
    alignSelf: 'center',
    borderRadius: 20,
    backgroundColor: '#fff',
  },
  itemRoom: {
    width: '80%',
    height: 50,
    borderRadius: 10,
    backgroundColor: '#3668ff',
    alignSelf: 'center',
    marginTop: 20,
    justifyContent: 'center',
  },
  txtRoom: {
    fontSize: 20,
    fontWeight: '700',
    alignSelf: 'center',
  },
});
