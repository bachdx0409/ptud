import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {useRoute} from '@react-navigation/native';
import Header from '../../common/Header';

const {width, height} = Dimensions.get('window');

const listItem = [
  {
    source: require('../../assets/image/combo1.png'),
    name: 'MY COMBO - 79.000đ',
    details:
      '1 bắp vừa + 1 nước lớn \n * Miễn phí đổi vị bắp Caramel * \n * Đổi vị phô mai thu tiền *',
  },
  {
    source: require('../../assets/image/combo2.png'),
    name: 'COMBO 2 NGƯỜI- 130.000đ',
    details:
      '1 bắp siêu lớn + 2 nước lớn \n * Miễn phí đổi vị bắp Caramel * \n * Miễn phí đổi vị bắp phô mai *',
  },
  {
    source: require('../../assets/image/combo3.png'),
    name: 'KEM BỎNG NGÔ - 49.000đ',
    details: '* Bỏng ngô vị mới - thử nghiệm *',
  },
  {
    source: require('../../assets/image/combo4.png'),
    name: 'COMBO BẮP - 99.000đ',
    details: '* 1 bắp phô mai vừa*\n* 1 bắp caramel vừa*\n* 1 bắp sô cô la*',
  },
];
const ChooseWaterScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const data = route.params;
  const [count, setCount] = useState(0);
  const [price, setPrice] = useState(data.total);
  console.log('data', data);
  var condition = count == 0 ? () => {} : () => setCount(count - 1);
  const listAdd = data.listAddItem;
  const roomId = data.roomId;

  const Item = props => {
    return (
      <View style={styles.boxItem}>
        <View style={styles.leftItem}>
          <Image style={{width: 120, height: 120}} source={props.source} />
        </View>
        <View style={styles.rightItem}>
          <View>
            <Text style={[styles.txtBtn, {fontSize: 16}]}>{props.name}</Text>
            <Text style={[styles.txtBtn, {fontSize: 14}]}>{props.details}</Text>
          </View>
          <View style={styles.circleBtn}>
            <TouchableOpacity style={styles.btn} onPress={condition}>
              <Text style={[styles.txtBtn, {fontSize: 16}]}>－</Text>
            </TouchableOpacity>
            <Text style={styles.txtBtn}>{count}</Text>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => setCount(count + 1)}>
              <Text style={styles.txtBtn}>﹢</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  return (
    <SafeAreaView>
      <Header title={`Phòng ${data.roomId}`} />
      <View style={styles.wrapper}>
        {listItem.map((item, index) => (
          <Item
            key={index}
            source={item.source}
            name={item.name}
            details={item.details}
          />
        ))}
        <View style={styles.box}>
          <View style={styles.wrapperBoxLeft}>
            <View style={styles.boxLeft}>
              <Text style={styles.textTitle}>VENOM 2: ĐỐI MẶT TỬ THÙ</Text>
              <Text style={{color: '#857a7d'}}>4D VietnamSub</Text>
            </View>
            <View style={[styles.boxLeft, {justifyContent: 'center'}]}>
              {/* <Text>{total}đ</Text> */}
              <Text>{price}đ</Text>
            </View>
          </View>
          <View style={styles.boxRight}>
            <TouchableOpacity
              onPress={() => navigation.navigate('Payment', {listAdd, roomId})}
              style={styles.wrapperBoxRight}>
              <Text style={styles.textBtn}>Thanh toán</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    width: width,
    height: height,
    alignItems: 'center',
    backgroundColor: '#15121F',
  },
  boxItem: {
    width: width,
    height: height * 0.15,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  rightItem: {
    width: '70%',
    height: '100%',
    paddingLeft: 10,
    justifyContent: 'space-around',
  },
  leftItem: {
    width: '30%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleBtn: {
    flexDirection: 'row',
    width: 100,
    height: 30,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  txtBtn: {
    fontSize: 20,
    fontWeight: '600',
    color: '#FFF',
  },
  btn: {
    width: 26,
    height: 26,
    borderRadius: 13,
    borderWidth: 2,
    borderColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    fontSize: 15,
    fontWeight: '600',
  },
  box: {
    width: width * 0.9,
    height: height * 0.1,
    marginTop: height * 0.05,
    borderRadius: 10,
    flexDirection: 'row',
    backgroundColor: '#f5f5f5',
  },
  wrapperBoxLeft: {
    width: '75%',
    height: '100%',
    paddingTop: 8,
    paddingLeft: 10,
  },
  wrapperBoxRight: {
    width: 80,
    height: 25,
    borderRadius: 20,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#d11f10',
  },
  boxLeft: {
    width: '100%',
    height: '50%',
  },
  boxRight: {
    width: '25%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBtn: {
    fontSize: 13,
    color: '#FFF',
    fontWeight: '600',
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
  },
});

export default ChooseWaterScreen;
