import React, {useState} from 'react';
import {
  View,
  Text,
  Alert,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import Header from '../../common/Header';
import {Agenda, AgendaEntry, AgendaSchedule} from 'react-native-calendars';

const timeToString = time => {
  const date = new Date(time);
  return date.toISOString().split('T')[0];
};

const {width, height} = Dimensions.get('window');
const MovieShowTimeScreen = () => {
  const [items, setItems] = useState([
    {
      '2021-12-28': [{name: 'item 1 - any js object'}],
    },
    {
      '2021-12-29': [{name: 'item 2 - any js object', height: 80}],
    },
    {
      '2021-12-30': [],
    },
    {
      '2021-12-31': [{name: 'item 3 - any js object'}, {name: 'any js object'}],
    },
  ]);

  const loadItems = day => {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = timeToString(time);

        if (!items[strTime]) {
          items[strTime] = [];

          //const numItems = Math.floor(Math.random() * 3 + 1);
          const numItems = 1;
          for (let j = 0; j < numItems; j++) {
            items[strTime].push({
              name: 'Item for ' + strTime + ' #' + j,
              height: Math.max(50, Math.floor(Math.random() * 150)),
              day: strTime,
            });
          }
        }
      }

      // const newItems: AgendaSchedule = {};
      Object.keys(items).forEach(key => {
        newItems[key] = items[key];
      });
      setItems(newItems);
    }, 1000);
  };
  // renderItem = (reservation: AgendaEntry) => {
  renderItem = () => {
    return (
      <TouchableOpacity
        style={[styles.item, {height: reservation.height}]}
        onPress={() => Alert.alert(reservation.name)}>
        <Text style={{}}>{reservation.name}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView>
      <Header title={'Phòng 1'} />
      <View style={styles.wrapper}>
        <Agenda
          items={items}
          loadItemsForMonth={loadItems}
          selected={'2021-12-29'}
          minDate={'2021-12-28'}
          pastScrollRange={2}
          futureScrollRange={12}
          renderItem={renderItem}
          // renderEmptyDate={this.renderEmptyDate}
          // rowHasChanged={this.rowHasChanged}
          // showClosingKnob={true}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    width: width,
    height: height,
  },
  item: {
    flex: 1,
    padding: 10,
    marginTop: 17,
    marginRight: 10,
    borderRadius: 5,
    backgroundColor: 'white',
  },
});

export default MovieShowTimeScreen;
