import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  ScrollView,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import Header from '../../common/Header';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useSelector} from 'react-redux';

const {width, height} = Dimensions.get('window');
const Section1 = props => {
  return (
    <View style={styles.boxSection1}>
      <Text style={{fontSize: 16, paddingLeft: 10}}>{props.titleSection1}</Text>
    </View>
  );
};
const Section2 = props => {
  return (
    <View style={styles.boxSection2}>
      <View style={styles.leftBoxSection2}>
        <Text>{props.titleSection2}</Text>
      </View>
      <Text style={styles.rightBoxSection2}>{props.txtValue}</Text>
    </View>
  );
};
const Section3 = props => {
  return (
    <View style={styles.boxSection2}>
      <View style={styles.leftBoxSection2}>
        <Image style={styles.imgSection} source={props.source} />
        <Text>{props.titleSection2}</Text>
      </View>
      <Text style={styles.rightBoxSection2}>{props.txtValue}</Text>
    </View>
  );
};
const PaymentScreen = () => {
  const navigation = useNavigation();
  const params = useRoute().params;
  const listAdd = params.listAdd || [];
  const roomId = params.roomId || '';
  const user = useSelector(state => state.slice.user);
  console.log('listAdd', listAdd);

  const pickTicket = async () => {
    console.log('listAdd', user);
    try {
      const myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');
      listAdd.map(item => {
        const raw = JSON.stringify({
          ticketId: item.id,
          customerId: user.id,
        });

        const requestOptions = {
          method: 'PUT',
          headers: myHeaders,
          body: raw,
          redirect: 'follow',
        };

        fetch('http://PTUDHTCinema.somee.com/api/pickTicket', requestOptions)
          .then(response => response.json())
          .then(result => {
            console.log(result);
          })
          .catch(error => console.log('error', error));
      });
      navigation.pop(4);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <SafeAreaView>
      <Header title={'Thanh toán'} />
      <View style={styles.wrapper}>
        <View style={styles.boxMain}>
          <View style={styles.leftBox}>
            <Image
              source={require('../../assets/image/venom2.jpeg')}
              style={styles.imgMovie}
            />
          </View>
          <View style={styles.rightBox}>
            <Text style={styles.nameMovie}>ĐỐI MẶT TỬ THÙ</Text>
            <Text style={styles.txtLimit}>Phim cấm khán giả dưới 13 tuổi</Text>
            <Text>Thứ 3, 28 Thg 12, 2021</Text>
            <Text>12:45 ~ 14:23</Text>
            <Text>Phòng số 2</Text>
            <Text>Ghế: A8, A9</Text>
            <Text style={styles.txtTotal}>Tổng thanh toán: 290.000đ</Text>
          </View>
        </View>
        <ScrollView>
          <Section1 titleSection1={'THÔNG TIN VÉ'} />
          <Section2 titleSection2={'Số lượng'} txtValue={'2'} />
          <Section2 titleSection2={'Tổng'} txtValue={'160.000đ'} />
          <Section1 titleSection1={'BẮP NƯỚC (TUỲ CHỌN)'} />
          <Section3
            titleSection2={'COMBO 2 NGƯỜI'}
            txtValue={'1'}
            source={require('../../assets/image/combo2.png')}
          />
          <Section2 titleSection2={'Tổng'} txtValue={'130.000đ'} />
          <Section1 titleSection1={'TỔNG KẾT'} />
          <Section2 titleSection2={'Tổng cộng'} txtValue={'290.000đ'} />
          <Section1 titleSection1={'THANH TOÁN'} />
          <TouchableOpacity>
            <Section3
              titleSection2={'ATM card (Thẻ nội địa)'}
              source={require('../../assets/icons/icAtm.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Section3
              titleSection2={'Ví momo'}
              source={require('../../assets/icons/icMomo.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Section3
              titleSection2={'ZaloPay'}
              source={require('../../assets/icons/icZalo.png')}
            />
          </TouchableOpacity>
          <View style={{width: width, alignItems: 'center', marginTop: 10}}>
            <TouchableOpacity onPress={pickTicket} style={styles.btn}>
              <Text>Đồng ý và tiếp tục</Text>
            </TouchableOpacity>
          </View>
          <View style={{width: width, height: 100}}></View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    width: width,
    height: height,
  },
  boxMain: {
    width: width,
    height: 160,
    flexDirection: 'row',
  },
  boxSection1: {
    width: width,
    height: 40,
    justifyContent: 'center',
    backgroundColor: '#d1d2d3',
  },
  boxSection2: {
    width: width,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderColor: '#a6a6a6',
    backgroundColor: '#FFF',
  },
  leftBox: {
    width: '30%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightBox: {
    width: '70%',
    height: '100%',
    padding: 10,
  },
  imgMovie: {width: 96, height: 142},
  nameMovie: {fontSize: 15, fontWeight: '600'},
  txtLimit: {marginTop: 5, color: '#ba2c2c'},
  txtTotal: {
    fontSize: 18,
    fontWeight: '600',
    color: '#ff0000',
    marginTop: 10,
  },
  leftBoxSection2: {
    fontSize: 16,
    paddingLeft: 10,
    width: '50%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightBoxSection2: {
    fontSize: 16,
    textAlign: 'right',
    paddingRight: 10,
    width: '50%',
  },
  imgSection: {
    width: 42,
    height: 42,
    marginRight: 5,
  },
  btn: {
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.8,
    height: 40,
    backgroundColor: '#47c9dede',
    borderRadius: 20,
  },
});

export default PaymentScreen;
