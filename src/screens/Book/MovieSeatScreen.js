import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Header from '../../common/Header';
import {useRoute} from '@react-navigation/native';
import {useNavigation} from '@react-navigation/core';

const {width, height} = Dimensions.get('window');

const MovieSeatScreen = props => {
  const navigation = useNavigation();
  const route = useRoute();
  const dataProps = route.params;
  const [count, setCount] = useState(0);
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  const [listAdd, setListAdd] = useState([]);
  console.log('dataProps', dataProps);
  const addTicket = (item, index, type) => {
    if (!type) {
      setCount(count - 1);
      setListAdd(listAdd.filter(a => a != index));
      setTotal(total - Number(item.price));
    } else {
      setCount(count + 1);
      setListAdd([...listAdd, index]);
      setTotal(total + Number(item.price));
    }
  };
  const listAddItem = listAdd.map(a => data[a]);

  useEffect(() => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    const filmId = dataProps.id;
    const roomId = dataProps.roomId;
    const dateIn = dataProps.dateIn.replace('T', ' ');
    console.log('roomId', roomId);
    console.log('filmId', filmId);
    fetch(
      `http://PTUDHTCinema.somee.com/api/ticket?filmId=${filmId}&roomId=${roomId}&dateIn=${dateIn}`,
      requestOptions,
    )
      .then(response => response.json())
      .then(result => {
        if (result?.data) {
          setData(result.data);
        }
      })
      .catch(error => console.log('error', error));
  }, []);

  const Seat = ({item, index}) => {
    let color = '#d8d8d8';
    let select = false;
    let type = true;
    if (item.customerId == 0) {
      color = '#6e6e6e';
      select = true;
    }
    if (listAdd.indexOf(index) != -1) {
      color = '#30ecff';
      type = false;
    }
    return (
      <TouchableOpacity
        disabled={!select}
        onPress={() => addTicket(item, index, type)}
        style={[styles.itemSeat, {backgroundColor: color}]}>
        <Text style={styles.seatPosition}>{index + 1}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <Header title={'Đặt vé'} />
      <View style={styles.wrapper}>
        <View style={styles.box1}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.typeSeat1}></View>
            <Text style={styles.textType}>Trống</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.typeSeat2}></View>
            <Text style={styles.textType}>Đang chọn</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.typeSeat3}></View>
            <Text style={styles.textType}>Đã đặt</Text>
          </View>
        </View>
        <View style={styles.TrapezoidStyle}></View>
        <View style={styles.wrapperSeat}>
          {data.map((item, index) => (
            <Seat index={index} item={item} />
          ))}
        </View>
        <View style={styles.box}>
          <View style={styles.wrapperBoxLeft}>
            <View style={styles.boxLeft}>
              {/* <Text style={styles.textTitle}>{route.params.params.title}</Text> */}
              <Text style={styles.textTitle}>{dataProps.name}</Text>
              <Text style={{color: '#857a7d'}}>
                {/* {route.params.params.type} VietnamSub */}
                4D VietnamSub
              </Text>
            </View>
            <View style={[styles.boxLeft, {justifyContent: 'center'}]}>
              <Text>{total}đ</Text>
            </View>
          </View>
          <View style={styles.boxRight}>
            <TouchableOpacity
              disabled={listAdd.length == 0}
              onPress={() =>
                navigation.navigate('ChooseWater', {
                  ...dataProps,
                  total,
                  listAddItem,
                })
              }
              style={styles.wrapperBoxRight}>
              <Text style={styles.textBtn}>Đặt vé</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: '#222222',
  },
  itemSeat: {
    width: 50,
    height: 40,
    borderWidth: 0.5,
    marginHorizontal: 5,
    marginVertical: 5,
    padding: 2,
    alignItems: 'center',
    backgroundColor: '#222223',
  },
  box1: {
    width: width * 0.8,
    height: height * 0.06,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: '#3b3b3b',
    borderRadius: 5,
    marginTop: height * 0.05,
  },
  TrapezoidStyle: {
    width: 250,
    height: 0,
    marginTop: 50,
    borderTopColor: '#FFF',
    borderTopWidth: 90,
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderRightColor: 'transparent',
    borderLeftColor: 'transparent',
    shadowColor: '#FFF',
    shadowOpacity: 1,
    shadowRadius: 8,
    marginBottom: 40,
  },
  itemSeat: {
    width: 16 * 1.4,
    height: 12 * 1.4,
    alignItems: 'center',
    justifyContent: 'center',
    // <<<<<<< HEAD
    marginTop: 5,
    marginLeft: 2,
    marginRight: 2,
    borderTopLeftRadius: 10 * 1.4,
    borderTopRightRadius: 10 * 1.4,
  },
  typeSeat1: {
    width: 16 * 1.3,
    height: 12 * 1.3,
    backgroundColor: '#6e6e6e',
    borderTopLeftRadius: 10 * 1.3,
    borderTopRightRadius: 10 * 1.3,
  },
  typeSeat2: {
    width: 16 * 1.3,
    height: 12 * 1.3,
    backgroundColor: '#30ecff',
    borderTopLeftRadius: 10 * 1.3,
    borderTopRightRadius: 10 * 1.3,
  },
  typeSeat3: {
    width: 16 * 1.3,
    height: 12 * 1.3,
    backgroundColor: '#d8d8d8',
    borderTopLeftRadius: 10 * 1.3,
    borderTopRightRadius: 10 * 1.3,
  },
  textType: {
    color: '#FFF',
    marginLeft: 2,
    fontSize: 14,
  },
  seatPosition: {
    fontSize: 10,
    fontWeight: '700',
    color: '#FFF',
  },
  wrapperSeat: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    width: width * 0.6,
    marginTop: height * 0.02,
    justifyContent: 'center',
  },
  test: {
    width: 100,
    height: 100,
    backgroundColor: 'pink',
  },
  textTitle: {
    fontSize: 15,
    fontWeight: '600',
  },
  box: {
    width: width * 0.9,
    height: height * 0.1,
    marginTop: height * 0.1,
    borderRadius: 10,
    flexDirection: 'row',
    backgroundColor: '#f5f5f5',
  },
  wrapperBoxLeft: {
    width: '75%',
    height: '100%',
    paddingTop: 8,
    paddingLeft: 10,
  },
  wrapperBoxRight: {
    width: 70,
    height: 25,
    borderRadius: 20,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#d11f10',
  },
  boxLeft: {
    width: '100%',
    height: '50%',
  },
  boxRight: {
    width: '25%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBtn: {
    fontSize: 13,
    color: '#FFF',
    fontWeight: '600',
    // =======
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
  },
  imageFilm: {
    width: '90%',
    height: 100,
    backgroundColor: '#ffff',
    alignSelf: 'center',
    marginTop: 40,
    borderTopRightRadius: 100,
    borderTopLeftRadius: 100,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
    // >>>>>>> 817655460b9f4e5b6969c526ef8b6b5038c8dc4c
  },
  txtScreen: {
    fontSize: 26,
    fontWeight: '500',
  },
});

export default MovieSeatScreen;
