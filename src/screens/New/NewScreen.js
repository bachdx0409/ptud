import React from 'react';
import {View, Text, SafeAreaView, StyleSheet, Dimensions} from 'react-native';

import Header from '../../common/Header';
const {width, height} = Dimensions.get('window');
const NewScreen = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Header title={'Tin mới & Ưu đãi'} />
      <View style={styles.wrapper}>
        <Text>Hello friends</Text>
      </View>
    </SafeAreaView>
  );
};

export default NewScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    width: width,
    height: height,
    backgroundColor: '#e5e5e5',
  },
});
