import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {useSelector} from 'react-redux';
import Header from '../../common/Header';

const {width, height} = Dimensions.get('window');

const ListMyTicket = [
  {
    title: 'VENOM 2: ĐỐI MẶT TỬ THÙ',
    date: '27 Thg 12, 2021',
    time: '2 giờ 15 phút',
    type: '4D',
    price: '80.000đ',
  },
  {
    title: 'JOHN WICK 3',
    date: '12 Thg 12, 2021',
    time: '2 giờ 10 phút',
    type: 'IMAX',
    price: '100.000đ',
  },
];

const MyTicketScreen = () => {
  const navigation = useNavigation();
  const [data, setData] = useState([]);
  const user = useSelector(state => state.slice.user);
  console.log('user', data);

  useEffect(() => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(
      `http://PTUDHTCinema.somee.com/api/getTicketOfCustomer?customerId=${user.id}`,
      requestOptions,
    )
      .then(response => response.json())
      .then(result => {
        setData(result.data);
      })
      .catch(error => console.log('error', error));
  }, []);

  const BoxTicket = ({item, index}) => {
    let timeIn = item.dateIn;
    timeIn = timeIn?.split('T') || [];
    const now = new Date();
    const timeDate = new Date(item.dateIn);
    let color = '#fff';
    let typeTxt = 'Chưa hết hạn';
    if (timeDate - now < 0) {
      color = 'rgba(0,0,0,0.5)';
      typeTxt = 'Đã hết hạn';
    }
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('TicketDetails', item)}
        style={{...styles.boxTicket, backgroundColor: color}}>
        <View style={styles.leftTicket}>
          <View style={styles.boxTitle}>
            <Text style={styles.textTitle}>{item.name}</Text>
          </View>
          <View style={styles.boxTime}>
            <Text>Ngày chiếu: {timeIn[0] || null} </Text>
            <Text>Giờ chiếu: {timeIn[1] || null} </Text>
          </View>
        </View>
        <View style={styles.rightTicket}>
          <View style={styles.boxType}>
            <Text style={styles.type}>{typeTxt}</Text>
          </View>
          <Text style={{fontWeight: '600'}}>{item.price}</Text>
        </View>
        <View style={styles.semiCircle}></View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Header title={'Vé của tôi'} />
      <View style={styles.wrapper}>
        <FlatList
          data={data}
          renderItem={BoxTicket}
          ListEmptyComponent={() => (
            <Text style={styles.txtEmpty}>Bạn chưa có vé nào</Text>
          )}
          showsVerticalScrollIndicator={false}
        />
      </View>
    </SafeAreaView>
  );
};

export default MyTicketScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  wrapper: {
    width: width,
    height: height,
    alignItems: 'center',
    backgroundColor: '#e5e5e5',
  },
  boxTicket: {
    width: width - 24,
    height: height * 0.14,
    backgroundColor: '#FFF',
    marginTop: 12,
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 1,
    borderColor: '#2fa2f9',
    borderStyle: 'dotted',
  },
  leftTicket: {
    width: '70%',
    height: '100%',
    borderRightWidth: 0.5,
    borderColor: '#2fa2f9',
  },
  boxTitle: {
    width: '100%',
    height: '50%',
  },
  textTitle: {
    padding: 10,
    fontSize: 14,
    fontWeight: '600',
  },
  boxTime: {
    width: '100%',
    height: '50%',
    paddingLeft: 10,
  },
  rightTicket: {
    width: '30%',
    height: '100%',
    paddingBottom: 5,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  semiCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#e5e5e5',
    borderRadius: 20,
    marginLeft: -20,
    marginTop: (height * 0.14 - 40) / 2,
  },
  type: {
    color: '#60d5d5',
    fontWeight: '600',
  },
  boxType: {
    width: '50%',
    height: '20%',
    borderWidth: 1,
    borderColor: '#2fa2f9',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtEmpty: {
    alignSelf: 'center',
    marginTop: '30%',
    fontSize: 15,
    fontWeight: '500',
  },
});
