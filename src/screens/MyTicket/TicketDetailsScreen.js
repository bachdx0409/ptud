import {useRoute} from '@react-navigation/native';
import React, {Component} from 'react';
import {View, Text, Dimensions, SafeAreaView, StyleSheet} from 'react-native';
import {Image} from 'react-native-animatable';

import Header from '../../common/Header';
const {width, height} = Dimensions.get('window');

const TicketDetailsScreen = () => {
  const params = useRoute().params;
  console.log('useRoute', params);
  let timeIn = params.dateIn;
  timeIn = timeIn.split('T');
  let timeOut = params.dateOut;
  timeOut = timeOut.split('T');
  const now = new Date();
  const timeDate = new Date(params.dateIn);
  let timestamp = new Date(params.dateOut) - new Date(params.dateIn);
  timestamp = timestamp / 1000 / 60;
  let txtTime = '';
  if (timestamp / 60 > 0) {
    const minu = timestamp - Math.floor(timestamp / 60) * 60;
    txtTime = timestamp / 60 + ' giờ' + '  ' + minu + ' phút';
  } else {
    txtTime = timestamp / 60 + ' phút';
  }
  let color = '#fff';
  let typeTxt = true;
  if (timeDate - now < 0) {
    color = 'rgba(0,0,0,0.5)';
    typeTxt = false;
  }
  return (
    <SafeAreaView style={styles.container}>
      <Header title={'Thông tin chi tiết'} />
      <View style={styles.box1}>
        <Text style={{fontSize: 16, fontWeight: '600'}}>{params.name}</Text>
        <Text style={{marginTop: 30, color: '#545454'}}>
          Ngày chiếu : {timeIn[0]}
        </Text>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.textInfo}>
            {timeIn[1]} ~ {timeOut[1]} /
          </Text>
          <Text style={{color: '#698281', marginTop: 5}}>{txtTime}</Text>
        </View>
        <Text style={styles.textInfo}>Ghế số {params.seat}</Text>
        <Text style={styles.textInfo}>Bắp nước</Text>
      </View>
      {typeTxt ? (
        <View style={styles.box2}>
          <Text>Vui lòng đưa mã số này đến quầy vé để nhận vé của bạn</Text>
          <Image
            source={require('../../assets/image/imgBarCode.png')}
            style={{
              width: 262.5,
              height: 72.8,
              marginTop: 10,
              marginBottom: 10,
            }}
          />
          <Text>
            Lưu ý: CNM không chấp nhận hoàn tiền hoặc đổi vé đã thanh toán thành
            công
          </Text>
        </View>
      ) : (
        <></>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  wrapper: {
    width: width,
    height: height,
    alignItems: 'center',
  },
  box1: {
    width: width,
    height: '40%',
    padding: 15,
    backgroundColor: '#DDDDDD',
  },
  box2: {
    width: width,
    height: '40%',
    paddingTop: 10,
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
  textInfo: {
    color: '#545454',
    marginTop: 5,
  },
});

export default TicketDetailsScreen;
