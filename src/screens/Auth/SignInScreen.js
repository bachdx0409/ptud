import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert,Modal,ActivityIndicator
} from 'react-native';
import API from '../../constants/API';
import { useDispatch } from 'react-redux';
import { setUser } from '../../redux/slice';
import AsyncStorage from '@react-native-async-storage/async-storage';

const {width, height} = Dimensions.get('window');

const SignInScreen = ({navigation}) => {
  const [phone, setPhone] = useState('');
  const [pass, setPass] = useState('');
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch()

  const onPressRegister = () => {
    navigation.navigate('SignUp');
  };

  const logIn = () => {
    setLoading(true);
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    const raw = JSON.stringify({
      phone: phone,
      password: pass,
    });

    const requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(`${API}/api/login`, requestOptions)
      .then(response => response.json())
      .then(async result => {
        if (result?.data?.id) {
          await AsyncStorage.setItem('@key', phone)
          await AsyncStorage.setItem('@key2', pass)
          setLoading(false);
          dispatch(setUser(result.data))
          
        } else {
          setLoading(false);
          Alert.alert('', result.message);
        }
      })
      .catch(error => {
        setLoading(false);
        console.log('error', error);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      {/* <Header /> */}
      <View style={styles.header}></View>
      <View style={styles.boxLogo}>
        <Image
          style={{width: 300, height: 100}}
          source={require('../../assets/icons/logoApp2.png')}
        />
        <Text style={styles.textTitle}>Xem phim thoả thích</Text>
      </View>
      <View
        style={{
          width: width * 0.75,
          height: height * 0.53,
          backgroundColor: '#c4f0fc',
          borderWidth: 1,
          borderColor: '#5CE1E6',
          borderRadius: 20,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={{width: '86%', height: '90%'}}>
          <Text style={{fontSize: 22, fontWeight: '700'}}>Đăng nhập</Text>
          <Text style={{fontWeight: '600', marginTop: 10, marginBottom: 5}}>
            Hãy đăng nhập và trải nghiệm dịch vụ của chúng tôi
          </Text>
          <TextInput
            style={styles.textInput}
            placeholder="Số điện thoại"
            value={phone}
            onChangeText={setPhone}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Mật khẩu"
            secureTextEntry
            value={pass}
            onChangeText={setPass}
          />
          <TouchableOpacity
            style={{
              width: '100%',
              marginTop: 10,
              alignItems: 'flex-end',
            }}>
            <Text style={{color: '#666666', fontSize: 12}}>Quên mật khẩu?</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={logIn} style={styles.btn}>
            <Text style={styles.textBtn}>Đăng nhập</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={onPressRegister} style={styles.btn2}>
            <Text style={styles.textBtn}>Đăng kí</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          width: 500,
          height: 500,
          borderRadius: 250,
          backgroundColor: '#5CE1E6',
          marginLeft: -width * 1.2,
        }}></View>
      <Modal transparent={true} visible={loading}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(0,0,0,0.6)',
          }}>
          <View style={styles.modalView}>
            <ActivityIndicator size="small" color="#0000ff" />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  wrapper: {
    width: width,
    height: height,
  },
  header: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: '#5CE1E6',
    marginLeft: width * 0.8,
    marginTop: -height * 0.2,
  },
  boxLogo: {
    width: width,
    height: height * 0.15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    color: '#636363',
    fontSize: 15,
    fontWeight: '700',
    marginTop: -30,
  },
  textInput: {
    width: '100%',
    height: '14%',
    marginTop: 15,
    paddingLeft: 10,
    borderRadius: 20,
    backgroundColor: '#FFF',
  },
  btn: {
    width: '100%',
    height: '14%',
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: '#000',
  },
  btn2: {
    width: '100%',
    height: '14%',
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: '#5CE1E6',
  },
  textBtn: {
    fontWeight: '600',
    color: '#FFF',
  },
  modalView: {
    width: 90,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
  },
});

export default SignInScreen;
