import React from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/core';

const {width, height} = Dimensions.get('window');

const FirstScreen = () => {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.boxLogo}>
        <Image
          style={{width: 300, height: 100}}
          source={require('../../assets/icons/logoApp2.png')}
        />
        <Text style={styles.textTitle}>Xem phim thoả thích</Text>
      </View>
      <View style={styles.space}></View>
      <View style={styles.wrapper}>
        <Text style={styles.text1}>Chào mừng bạn</Text>
        <Text style={styles.text2}>
          Bạn đã có tài khoản chưa, hãy chọn đăng ký nếu chưa có tài khoản
        </Text>
        <View style={styles.wrapperBtn}>
          <TouchableOpacity
            onPress={() => navigation.navigate('SignIn')}
            style={styles.btn1}>
            <Text style={styles.textBtn1}>Đăng nhập</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('SignUp')}
            style={styles.btn2}>
            <Text style={styles.textBtn2}>Đăng ký</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  boxLogo: {
    width: width,
    height: height * 0.15,
    marginTop: height * 0.12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    color: '#636363',
    fontSize: 15,
    fontWeight: '700',
    marginTop: -30,
  },
  wrapper: {
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    width: width,
    height: height * 0.5,
    alignItems: 'center',
    backgroundColor: '#5CE1E6',
  },
  space: {
    width: width,
    height: height * 0.2,
  },
  text1: {
    fontSize: 30,
    fontWeight: '700',
    width: width * 0.9,
    marginTop: height * 0.08,
    marginBottom: height * 0.02,
  },
  text2: {
    width: width * 0.9,
    fontSize: 15,
    fontWeight: '600',
  },
  wrapperBtn: {
    width: width * 0.9,
    height: height * 0.1,
    marginTop: height * 0.05,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  btn1: {
    width: width * 0.4,
    height: height * 0.06,
    backgroundColor: '#000',
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn2: {
    width: width * 0.4,
    height: height * 0.06,
    backgroundColor: '#FFF',
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtn1: {
    fontWeight: '600',
    color: '#FFF',
  },
  textBtn2: {
    fontWeight: '600',
    color: '#000',
  },
});

export default FirstScreen;
