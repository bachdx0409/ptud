import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
} from 'react-native';
import {launchImageLibrary} from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import Header from '../../common/Header';
import API from '../../constants/API';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const {width, height} = Dimensions.get('window');

const SignUpScreen = () => {
  const [phone, setPhone] = useState('');
  const [name, setName] = useState('');
  const [pass, setPass] = useState('');
  const [cPass, setCPass] = useState('');
  const [image, setImage] = useState('');
  const [loading, setLoading] = useState(false);

  const onPressAdd = () => {
    launchImageLibrary({}, response => {
      if (response.didCancel) {
      } else if (response.error) {
        alert('Lỗi', error);
      } else {
        setImage(response.assets[0]);
      }
    });
  };

  const validate = () => {
    if (phone == '') {
      return {
        status: false,
        mess: 'Bạn phải nhập số điện thoại',
      };
    } else if (name == '') {
      return {
        status: false,
        mess: 'Bạn phải nhập tên hiển thị',
      };
    } else if (pass == '') {
      return {
        status: false,
        mess: 'Bạn phải nhập mật khẩu',
      };
    } else if (pass.length < 6) {
      return {
        status: false,
        mess: 'Mật khẩu quá ngắn',
      };
    } else if (cPass !== pass) {
      return {
        status: false,
        mess: 'Mật khẩu xác nhận không khớp',
      };
    } else if (image == null || image == undefined) {
      return {
        status: false,
        mess: 'Bạn phải chọn ảnh đại diện',
      };
    }
    {
      return {
        status: true,
      };
    }
  };

  const submit = async () => {
    const check = validate();
    if (check.status) {
      setLoading(true);
      const timeStamp = new Date();
      const reference = storage().ref(`${timeStamp}.png`);
      await reference.putFile(image.uri);
      const url = await storage().ref(`${timeStamp}.png`).getDownloadURL();

      let myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');
      let raw = JSON.stringify({
        phone: phone,
        password: pass,
        name: name,
        image: url,
      });

      let requestOptions = {
        headers: myHeaders,
        method: 'POST',
        body: raw,
        redirect: 'follow',
      };

      fetch(`${API}/api/createCustomer`, requestOptions)
        .then(response => response.json())
        .then(result => {
          setLoading(false);
          alert(result.message);
        })
        .catch(error => {
          setLoading(false);
          console.log('error', error);
        });
    } else {
      setLoading(false);
      alert(check.mess);
    }
  };

  return (
    <View style={{width: '100%', height: Dimensions.get('window').height}}>
      <KeyboardAwareScrollView>
        <SafeAreaView style={styles.container}>
          <Header />
          <View style={styles.header}></View>
          <View style={styles.boxLogo}>
            <Image
              style={{width: 300, height: 100}}
              source={require('../../assets/icons/logoApp2.png')}
            />
            <Text style={styles.textTitle}>Xem phim thoả thích</Text>
          </View>
          <View
            style={{
              width: width * 0.75,
              height: height * 0.58,
              backgroundColor: '#c4f0fc',
              borderWidth: 1,
              borderColor: '#5CE1E6',
              borderRadius: 20,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{width: '86%', height: '90%'}}>
              <Text style={{fontSize: 22, fontWeight: '700'}}>Đăng ký</Text>
              <Text style={{fontWeight: '600', marginTop: 10}}>
                Bạn chưa có tài khoản? hãy đăng ký
              </Text>
              <TextInput
                style={styles.textInput}
                placeholder="Số điện thoại"
                value={phone}
                onChangeText={setPhone}
              />
              <TextInput
                style={styles.textInput}
                placeholder="Tên hiển thị"
                value={name}
                onChangeText={setName}></TextInput>
              <TextInput
                style={styles.textInput}
                placeholder="Mật khẩu"
                secureTextEntry
                value={pass}
                onChangeText={setPass}></TextInput>
              <TextInput
                style={styles.textInput}
                placeholder="Nhập lại mật khẩu"
                secureTextEntry
                value={cPass}
                onChangeText={setCPass}></TextInput>
              <TouchableOpacity
                onPress={onPressAdd}
                style={styles.boxSelectImage}>
                {image?.uri ? (
                  <Image style={styles.imageAvt} source={{uri: image?.uri}} />
                ) : (
                  <Text style={styles.txtAdd}>+</Text>
                )}
              </TouchableOpacity>
              <TouchableOpacity onPress={submit} style={styles.btn}>
                <Text style={styles.textBtn}>Đăng ký</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              width: 500,
              height: 500,
              borderRadius: 250,
              backgroundColor: '#5CE1E6',
              marginLeft: -width * 1.2,
              zIndex: -1,
            }}></View>
          <Modal transparent={true} visible={loading}>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'rgba(0,0,0,0.6)',
              }}>
              <View style={styles.modalView}>
                <ActivityIndicator size="small" color="#0000ff" />
              </View>
            </View>
          </Modal>
        </SafeAreaView>
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  wrapper: {
    width: width,
    height: height,
  },
  header: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: '#5CE1E6',
    marginLeft: width * 0.8,
    marginTop: -height * 0.2,
  },
  boxLogo: {
    width: width,
    height: height * 0.15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    color: '#636363',
    fontSize: 15,
    fontWeight: '700',
    marginTop: -30,
  },
  textInput: {
    width: '100%',
    height: 50,
    marginTop: 15,
    paddingLeft: 10,
    borderRadius: 20,
    backgroundColor: '#FFF',
  },
  btn: {
    width: '100%',
    height: 40,
    marginTop: 25,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: '#000',
    marginTop: 30,
  },
  textBtn: {
    fontWeight: '600',
    color: '#FFF',
  },
  boxSelectImage: {
    width: 60,
    height: 60,
    marginTop: 10,
    borderRadius: 5,
    borderWidth: 1,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  txtAdd: {
    alignSelf: 'center',
    fontSize: 24,
    fontWeight: '500',
  },
  imageAvt: {
    width: '100%',
    height: '100%',
    borderRadius: 5,
  },
  modalView: {
    width: 90,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
  },
});

export default SignUpScreen;
