import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  TextInput,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import Header from '../../common/Header';

const {width, height} = Dimensions.get('window');

const Section1 = props => {
  return (
    <View style={styles.section1}>
      <Text style={styles.text1}>{props.titleSection}</Text>
    </View>
  );
};
const Section2 = props => {
  return (
    <View style={styles.section2}>
      <Text style={styles.text2}>{props.titleName}</Text>
      <Text style={styles.text3}>{props.name}</Text>
    </View>
  );
};

const InfoScreen = () => {
  const [modalVisible, setModalVisible] = useState(false);

  const CustomModal = () => {
    return (
      <Modal visible={modalVisible} transparent={true} animationType="slide">
        <View style={stylesModal.centeredView}>
          <View style={stylesModal.modalView}>
            <View style={stylesModal.wrppModal}>
              <Text style={{fontSize: 18, fontWeight: '600'}}>
                Thay đổi thông tin
              </Text>
              <TextInput
                maxLength={11}
                keyboardType="numeric"
                placeholder="Số điện thoại"
                style={stylesModal.textInput}
              />
              <TextInput
                placeholder="Địa chỉ"
                style={[stylesModal.textInput, {marginTop: 5}]}
              />
              <TouchableOpacity
                onPress={() => {}}
                style={[stylesModal.cancel, {backgroundColor: '#47c9dede'}]}>
                <Text style={[stylesModal.btnCancel, {color: '#FFF'}]}>
                  Xác nhận
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setModalVisible(!modalVisible)}
                style={stylesModal.cancel}>
                <Text style={stylesModal.btnCancel}>Thoát</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <CustomModal />
      <View style={styles.wrapper}>
        <Header title={'Thông tin tài khoản'} />
        <View style={styles.main}>
          <View style={styles.a}>
            <Text style={{fontSize: 16, padding: 10}}>Tài khoản</Text>
          </View>
          <Text style={{fontSize: 15, padding: 10}}>
            hainamdoremon@gmail.com
          </Text>
          <Section1 titleSection={'Thông tin thêm'} />
          <Section2 titleName={'Họ tên:'} name={'NamLifebuoy'} />
          <Section2 titleName={'Ngày sinh'} name={'01/01/2000'} />
          <Section2 titleName={'Giới tính'} name={'Nam'} />
          <Section1 titleSection={'Liên hệ'} />
          <Section2 titleName={'Số điện thoại'} name={'0999012789'} />
          <Section2 titleName={'Địa chỉ'} name={'Hà Nội'} />
          <View style={styles.wrapperBtn}>
            <TouchableOpacity
              onPress={() => setModalVisible(!modalVisible)}
              style={styles.btn}>
              <Text style={styles.textBtn}>Thay đổi thông tin</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightyellow',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    paddingTop: 10,
    alignItems: 'center',
  },
  main: {
    marginTop: 15,
    width: width * 0.9,
    height: height * 0.6,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#8ED2F0',
    backgroundColor: '#FFF',
  },
  section1: {
    width: '100%',
    height: height * 0.05,
    backgroundColor: '#d1d2d3',
  },
  a: {
    width: '100%',
    height: height * 0.05,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#d1d2d3',
  },
  section2: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#d9dada',
  },
  text1: {fontSize: 16, padding: 10},
  text2: {fontSize: 15, padding: 10, width: '35%'},
  text3: {
    fontSize: 15,
    padding: 10,
    width: '65%',
    textAlign: 'right',
  },
  wrapperBtn: {
    width: '100%',
    alignItems: 'center',
    marginTop: 40,
  },
  btn: {
    width: width * 0.65,
    height: height * 0.06,
    backgroundColor: '#47c9dede',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
  },
  textBtn: {
    color: '#FFF',
    fontSize: 15,
    fontWeight: '600',
  },
});
const stylesModal = StyleSheet.create({
  centeredView: {
    flex: 1,
    //marginTop: 52,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalView: {
    margin: 50,
    backgroundColor: '#FFF',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  wrppModal: {
    width: width * 0.6,
    height: height * 0.3,
    alignItems: 'center',
  },
  textInput: {
    width: width * 0.5,
    height: height * 0.05,
    backgroundColor: '#ececec',
    borderRadius: 25,
    paddingLeft: 10,
    marginTop: 20,
  },
  confirm: {},
  cancel: {
    width: width * 0.5,
    height: height * 0.05,
    marginTop: 5,
    borderWidth: 1,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
    borderColor: '#47c9dede',
  },
  btnCancel: {
    fontSize: 15,
    fontWeight: '600',
    color: '#47c9dede',
  },
});

export default InfoScreen;
