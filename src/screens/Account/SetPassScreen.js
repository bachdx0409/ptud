import React, {useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {Image} from 'react-native-animatable';
import Header from '../../common/Header';

const {width, height} = Dimensions.get('window');

const SetPassScreen = () => {
  const [isSecureEntry, setIsSecureEntry] = useState(true);
  const [isSecureEntryConfirm, setIsSecureEntryConfirm] = useState(true);
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <Header title={'Mật mã thanh toán'} />
        <View style={styles.box1}>
          <Text>MẬT MÃ THANH TOÁN</Text>
          <Text style={{marginTop: 10}}>
            - Mật mã thanh toán dùng để xác minh các giao dịch thanh toán bằng
            Voucher, Đổi điểm thưởng...
          </Text>
          <Text>
            - Mật mã thanh toán bao gồm 6 chữ số và không được là 6 số trùng
            nhau (VD: 999999)
          </Text>
        </View>
        <View style={styles.box2}>
          <Text style={styles.textBox}>Mật mã mới</Text>
          <View style={styles.boxTextIn}>
            <TextInput
              style={styles.textInput}
              keyboardType="numeric"
              maxLength={6}
            />
            <Image
              source={require('../../assets/icons/icEye.png')}
              style={styles.showPass}
            />
          </View>
        </View>
        <View style={styles.box2}>
          <Text style={styles.textBox}>Nhập lại mật mã mới</Text>
          <View style={styles.boxTextIn}>
            <TextInput
              style={styles.textInput}
              keyboardType="numeric"
              maxLength={6}
            />
            <Image
              source={require('../../assets/icons/icEye.png')}
              style={styles.showPass}
            />
          </View>
        </View>
        <TouchableOpacity style={styles.btn}>
          <Text style={{fontSize: 16, fontWeight: '600', color: '#FFF'}}>
            Đồng ý
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'lightyellow',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    paddingTop: 10,
    alignItems: 'center',
  },
  box1: {
    width: width,
    height: height * 0.15,
    backgroundColor: '#8ED2F0',
    padding: 15,
  },
  box2: {
    width: width,
    height: height * 0.05,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#8ED2F0',
    backgroundColor: '#FFF',
  },
  textInput: {
    width: '70%',
    textAlign: 'right',
  },
  textBox: {
    fontSize: 16,
    padding: 10,
    width: '50%',
  },
  boxTextIn: {
    width: '50%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  showPass: {
    width: 20,
    height: 20,
  },
  btn: {
    width: width * 0.8,
    height: height * 0.05,
    marginTop: 20,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#46bedb',
  },
});

export default SetPassScreen;
