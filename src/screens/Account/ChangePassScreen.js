import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {color} from 'react-native-reanimated';
import Header from '../../common/Header';

const {width, height} = Dimensions.get('window');

const ChangePassScreen = () => {
  const [isSecureEntryOld, setIsSecureEntryOld] = useState(true);
  const [isSecureEntry, setIsSecureEntry] = useState(true);
  const [isSecureEntryConfirm, setIsSecureEntryConfirm] = useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <Header title={'Đổi mật khẩu'} />
        <View style={styles.icon}>
          <Image
            style={{width: 170, height: 170}}
            source={require('../../assets/icons/icChangePass.png')}
          />
        </View>
        <View style={styles.boxMain}>
          {/* box1 */}
          <View style={styles.box}>
            <Text style={{color: '#2fa2f9', fontSize: 15}}>
              Mật khẩu hiện tại
            </Text>
            <View style={styles.row}>
              <TextInput
                style={styles.textInput}
                secureTextEntry={isSecureEntryOld}
              />
              <TouchableOpacity
                onPress={() => setIsSecureEntryOld(!isSecureEntryOld)}
                style={styles.center}>
                <Image
                  source={require('../../assets/icons/icEye.png')}
                  style={styles.showPass}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* box2 */}
          <View style={styles.box}>
            <Text style={{color: '#2fa2f9', fontSize: 15}}>Mật khẩu mới</Text>
            <View style={styles.row}>
              <TextInput
                style={styles.textInput}
                secureTextEntry={isSecureEntry}
              />
              <TouchableOpacity
                onPress={() => setIsSecureEntryOld(!isSecureEntry)}
                style={styles.center}>
                <Image
                  source={require('../../assets/icons/icEye.png')}
                  style={styles.showPass}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* box3 */}
          <View style={styles.box}>
            <Text style={{color: '#2fa2f9', fontSize: 15}}>
              Xác nhận mật khẩu
            </Text>
            <View style={styles.row}>
              <TextInput
                style={styles.textInput}
                secureTextEntry={isSecureEntryConfirm}
              />
              <TouchableOpacity
                onPress={() => setIsSecureEntryOld(!isSecureEntryConfirm)}
                style={styles.center}>
                <Image
                  source={require('../../assets/icons/icEye.png')}
                  style={styles.showPass}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* abc */}
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.textBtn}>Xác nhận</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightyellow',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    paddingTop: 10,
    alignItems: 'center',
  },
  icon: {
    marginTop: 30,
    width: 180,
    height: 180,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 90,
    borderColor: '#8ED2F0',
    backgroundColor: '#FFF',
  },
  boxMain: {
    marginTop: height * 0.05,
    width: width * 0.8,
    height: height * 0.45,
    backgroundColor: '#FFF',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#8ED2F0',
    alignItems: 'center',
    paddingTop: 20,
  },
  row: {
    flexDirection: 'row',
  },
  textInput: {
    width: '100%',
    height: 40,
    borderBottomWidth: 1,
    borderColor: '#2fa2f9',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: '90%',
    height: '15%',
    marginTop: 20,
    //backgroundColor: 'lightgreen',
  },
  showPass: {
    width: 20,
    height: 20,
    marginLeft: -35,
  },
  btn: {
    width: '50%',
    height: '12%',
    marginTop: 25,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3ee9f3',
  },
  textBtn: {
    fontWeight: '600',
    fontSize: 16,
    color: '#fff',
  },
});

export default ChangePassScreen;
