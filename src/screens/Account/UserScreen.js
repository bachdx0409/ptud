import React from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Header from '../../common/Header';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';
import {setUser} from '../../redux/slice';
import {useSelector} from 'react-redux';

const {width, height} = Dimensions.get('window');
const icon1 = require('../../assets/icons/icInformation.png');
const icon2 = require('../../assets/icons/icLock.png');
const icon3 = require('../../assets/icons/icPass.png');
const icon4 = require('../../assets/icons/icHistory.png');
const icon5 = require('../../assets/icons/icGiftCard.png');
const icon6 = require('../../assets/icons/icCoin.png');
const icon7 = require('../../assets/icons/icMemberCard.png');

const UserScreen = ({navigation}) => {
  const user = useSelector(state => state.slice.user);
  const dispatch = useDispatch();

  const setLogout = async () => {
    await AsyncStorage.setItem('@key', '');
    await AsyncStorage.setItem('@key2', '');
    dispatch(setUser(null));
  };

  const logout = () => {
    Alert.alert('', 'Bạn có muốn đăng xuất?', [
      {
        text: 'Huỷ',
      },
      {text: 'Đăng xuất', onPress: () => setLogout()},
    ]);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <Header title={'Thành viên CNM'} />
        <View style={{alignItems: 'center'}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={{uri: user?.image}} style={styles.avt} />
            <TouchableOpacity>
              <Image
                source={require('../../assets/icons/icCamera.png')}
                style={styles.icCamera}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.userName}>{user?.name}</Text>
          <View style={styles.boxMain}>
            <View style={styles.horizontal1}>
              <View style={styles.box1_1}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Info')}
                  style={{alignItems: 'center'}}>
                  <Image
                    source={icon1}
                    style={{width: 30, height: 30, marginBottom: 5}}
                  />
                  <Text style={styles.textBox1}>Thông tin tài khoản</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.box1_2}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Change')}
                  style={{alignItems: 'center'}}>
                  <Image
                    source={icon2}
                    style={{width: 30, height: 30, marginBottom: 5}}
                  />
                  <Text style={styles.textBox1}>Đổi mật khẩu</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.box1_3}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('SetPass')}
                  style={{alignItems: 'center'}}>
                  <Image
                    source={icon3}
                    style={{width: 30, height: 30, marginBottom: 5}}
                  />
                  <Text style={styles.textBox2}>Mật mã thanh toán</Text>
                </TouchableOpacity>
              </View>
            </View>
            {/* /// */}
            <View style={styles.horizontal2}>
              <View style={styles.box2_1}>
                <TouchableOpacity
                  style={{
                    alignItems: 'center',
                  }}>
                  <Image
                    source={icon4}
                    style={{width: 30, height: 30, marginBottom: 5}}
                  />
                  <Text style={styles.textBox2}>Lịch sử giao dịch</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.box2_2}>
                <TouchableOpacity
                  style={{
                    alignItems: 'center',
                  }}>
                  <Image
                    source={icon6}
                    style={{width: 30, height: 30, marginBottom: 5}}
                  />
                  <Text style={styles.textBox2}>Điểm thưởng</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.box2_3}>
                <TouchableOpacity
                  style={{
                    alignItems: 'center',
                  }}>
                  <Image
                    source={icon5}
                    style={{width: 30, height: 30, marginBottom: 5}}
                  />
                  <Text style={styles.textBox2}>Thẻ quà tặng</Text>
                </TouchableOpacity>
              </View>
            </View>
            {/* /// */}
            <View style={styles.horizontal3}>
              <View style={styles.box3_1}>
                <TouchableOpacity
                  onPress={logout}
                  style={{
                    alignItems: 'center',
                  }}>
                  <Image
                    source={icon7}
                    style={{width: 30, height: 30, marginBottom: 5}}
                  />
                  <Text style={styles.textBox2}>Đăng xuất</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.box3_2}></View>
              <View style={styles.box3_3}></View>
            </View>
            {/* /// */}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default UserScreen;

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightyellow',
  },
  wrapper: {
    width: '100%',
    height: '100%',
    paddingTop: 10,
    backgroundColor: 'lightyellow',
    alignItems: 'center',
  },
  avt: {
    width: 120,
    height: 120,
    borderRadius: 60,
    borderWidth: 2,
    borderColor: '#8ED2F0',
  },
  icCamera: {
    width: 26,
    height: 26,
    marginLeft: -26,
    marginTop: 94,
  },
  userName: {
    fontSize: 20,
    marginTop: 10,
    color: '#2fa2f9',
    fontWeight: '600',
  },
  boxMain: {
    marginTop: height * 0.05,
    width: width * 0.8,
    height: height * 0.5,
    backgroundColor: '#FFF',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#8ED2F0',
  },
  horizontal1: {
    flexDirection: 'row',
    width: width * 0.8,
    height: height * (0.5 / 3),
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  horizontal2: {
    flexDirection: 'row',
    width: width * 0.8,
    height: height * (0.5 / 3),
    justifyContent: 'center',
  },
  horizontal3: {
    flexDirection: 'row',
    width: width * 0.8,
    height: height * (0.5 / 3),
    justifyContent: 'center',
  },
  box1_1: {
    width: width * (0.8 / 3.5),
    height: height * (0.5 / 4),
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#8ED2F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box1_2: {
    width: width * (0.8 / 3),
    height: height * (0.5 / 4),
    borderBottomWidth: 1,
    borderColor: '#8ED2F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box1_3: {
    width: width * (0.8 / 3.5),
    height: height * (0.5 / 4),
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#8ED2F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box2_1: {
    width: width * (0.8 / 3.5),
    height: height * (0.5 / 3),
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#8ED2F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box2_2: {
    width: width * (0.8 / 3),
    height: height * (0.5 / 3),
    borderBottomWidth: 1,
    borderColor: '#8ED2F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box2_3: {
    width: width * (0.8 / 3.5),
    height: height * (0.5 / 3),
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#8ED2F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box3_1: {
    width: width * (0.8 / 3.5),
    height: height * (0.5 / 4),
    borderRightWidth: 1,
    borderColor: '#8ED2F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box3_2: {
    width: width * (0.8 / 3),
    height: height * (0.5 / 4),
    justifyContent: 'center',
    alignItems: 'center',
  },
  box3_3: {
    width: width * (0.8 / 3.5),
    height: height * (0.5 / 4),
    borderLeftWidth: 1,
    borderColor: '#8ED2F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBox1: {
    width: 70,
    height: 35,
    color: '#2fa2f9',
    textAlign: 'center',
  },
  textBox2: {
    width: 90,
    height: 35,
    color: '#2fa2f9',
    textAlign: 'center',
    paddingHorizontal: 10,
  },
});
