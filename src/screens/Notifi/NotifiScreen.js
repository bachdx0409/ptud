import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const NotifiScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={{alignSelf: 'center'}}>NotifiScreen</Text>
    </View>
  );
};

export default NotifiScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});
