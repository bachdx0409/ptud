import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface CounterState {
    value: number,
    role:number | undefined,
    user:any
}

const initialState: CounterState = {
    value: 0,
    role: undefined,
    user:undefined,
}

export const slice = createSlice({
    name: 'slice',
    initialState,
    reducers: {
        setUser: (state,action) => {
            state.user = action.payload
        },
    },
})

// Action creators are generated for each case reducer function
export const { setUser, } = slice.actions

export default slice.reducer