import React from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native';

const CustomInput = props => {
  return (
    <View>
      <Text style={styles.txtTitle}>{props.title}</Text>
      <TouchableOpacity style={styles.input} onPress={props.onPress} >
        <Text style={styles.txt} >{props?.value || props.placeholder }</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CustomInput;

const styles = StyleSheet.create({
  txt:{
    marginLeft:10
  },
  txtTitle: {
    marginLeft: '5%',
    fontWeight: '500',
    fontSize: 15,
    marginTop: 10,
  },
  input: {
    width: '90%',
    height: 40,
    borderWidth: 0.6,
    borderRadius: 5,
    alignSelf: 'center',
    marginTop: 5,
    marginBottom:3,
    justifyContent:'center'
  },
});
