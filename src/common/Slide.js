import React, {Component, useEffect} from 'react';
import {View, Text, Animated, Alert} from 'react-native';

const SlideAnimated2 = props => {
  const fade = new Animated.Value(0);
  
  useEffect(() => {
    Animated.timing(fade, {
      toValue: 1,
      duration: 1000,
      delay: props?.delay,
      useNativeDriver: true,
    }).start();
  }, [props]);

  return (
    <Animated.View style={[props.style, {opacity: fade}]}>
      {props.children}
    </Animated.View>
  );
};

export default class SlideAnimated extends Component {
  constructor(props) {
    super(this.props);
    this.state = {fade: new Animated.Value(0)};
  }

  shouldComponentUpdate() {
    Animated.timing(this.state.fade, {
      toValue: 1,
      duration: 1000,
      delay: props?.delay,
      useNativeDriver: true,
    }).start();
  }

  render() {
    
    return (
      <Animated.View style={[this.props.style, {opacity: this.state.fade}]}>
        {this.props.children}
      </Animated.View>
    );
  }
}
