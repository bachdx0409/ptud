import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { useNavigation } from '@react-navigation/native';

interface props {
    title: string
}

const Header = (props: props) => {
    const { title } = props
    const navigation = useNavigation()
    return (
        <View style={styles.container} >
            <TouchableOpacity onPress={() => navigation.goBack()} >
                <Image style={styles.btBack}
                    source={require('../assets/icons/icLeftArrow.png')} />
            </TouchableOpacity>
            <Text style={styles.title} >{title}</Text>
            <View style={styles.btBack} />
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 50,
        paddingHorizontal: '5%',
        paddingTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    btBack: {
        width: 25,
        height: 25,
    },
    title: {
        fontSize: 18,
        fontWeight: '500',
    }
})
