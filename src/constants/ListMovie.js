import {useNavigation} from '@react-navigation/native';

const navigation = useNavigation();

export const ListMovie = [
  {
    title: 'VENOM 2: ĐỐI MẶT TỬ THÙ',
    img: require('../assets/image/venom2.jpeg'),
    date: '27 Thg 12, 2021',
    time: '2 giờ 15 phút',
    type: '4D',
    press: () => navigation.navigate('MovieDetails', {title: 'nam'}),
    description: `Venom 2: Đối mặt tử thù là bộ phim tiếp nối câu chuyện ở phần 1,
    sau khi Venom sống sót và quyết định ở lại Trái Đất cùng anh chàng
    Eddie vì yêu quý hành tinh này. Nút thắt tới khi Venom chê cười
    Eddie khiến họ xô xát và chiến tranh lạnh. Đúng lúc này sát nhân
    Celtus đã hoá Venom đỏ, sát hại lính canh và thoát khỏi nhà tù.`,
    genre: 'Hành động',
    director: 'Andy Serkis',
    actor: 'Tom Hardy, Michelle Williams, Naomie Harris',
    duration: '1 giờ 37 phút',
    language: 'Tiếng Anh - Phụ đề Tiếng Việt',
  },
  {
    title: 'BLACK WINDOW',
    img: require('../assets/image/blackWindow.jpeg'),
    date: '19 Thg 12, 2021',
    time: '1 giờ 15 phút',
    type: 'IMAX',
  },
  {
    title: 'SNAKE EYES',
    img: require('../assets/image/snakeEyes.jpeg'),
    date: '26 Thg 12, 2021',
    time: '2 giờ 15 phút',
    type: '2D',
  },
  {
    title: 'JOHN WICH 3',
    img: require('../assets/image/johnWick3.jpeg'),
    date: '12 Thg 12, 2021',
    time: '3 giờ 15 phút',
    type: 'IMAX',
  },
  {
    title: "THE KING'S MAN",
    img: require('../assets/image/theKingsMan.jpeg'),
    date: '12 Thg 12, 2021',
    time: '3 giờ 15 phút',
    type: '4D',
  },
  {
    title: 'CUỘC CHIẾN SINH TỬ',
    img: require('../assets/image/mortalKombat.jpeg'),
    date: '12 Thg 12, 2021',
    time: '3 giờ 15 phút',
    type: '4D',
  },
  {
    title: 'CUỘC CHIẾN SINH TỬ',
    img: require('../assets/image/mortalKombat.jpeg'),
    date: '12 Thg 12, 2021',
    time: '3 giờ 15 phút',
    type: '4D',
  },
];
