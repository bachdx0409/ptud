import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';
import {Image, StyleSheet} from 'react-native';
import UserScreen from '../screens/Account/UserScreen';
import CinemaScreen from '../screens/Cinema/CinemaScreen';
import HomeScreen from '../screens/Home/HomeScreen';
import FirstScreen from '../screens/Auth/FirstScreen';
import SignUpScreen from '../screens/Auth/SignUpScreen';
import SignInScreen from '../screens/Auth/SignInScreen';
import SelectMovieScreen from '../screens/Book/SelectMovieScreen';
import MyTicketScreen from '../screens/MyTicket/MyTicketScreen';
import NewScreen from '../screens/New/NewScreen';
import NotifiScreen from '../screens/Notifi/NotifiScreen';
import CustomDrawer from '../assets/components/CustomDrawer';
import InfoScreen from '../screens/Account/InfoScreen';
import ChangePassScreen from '../screens/Account/ChangePassScreen';
import SetPassScreen from '../screens/Account/SetPassScreen';
import MovieDetailsScreen from '../screens/Book/MovieDetaisScreen';
import MovieSeatScreen from '../screens/Book/MovieSeatScreen';
import TicketDetailsScreen from '../screens/MyTicket/TicketDetailsScreen';
import ManaRoom from '../screens/Manager/ManaRoom';
import ListRoomScreen from '../screens/Manager/ListRoomScreen';
import ManaFilm from '../screens/Manager/ManaFilm';
import {useSelector, useDispatch} from 'react-redux';
import API from '../constants/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {setUser} from '../redux/slice';
import MovieShowTimeScreen from '../screens/Book/MovieShowTimeScreen';
import ChooseWaterScreen from '../screens/Book/ChooseWaterScreen';
import PaymentScreen from '../screens/Book/PaymentScreen';

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const listIcon = [
  require('../assets/icons/icHome.png'),
  require('../assets/icons/icFilm.png'),
  require('../assets/icons/icUser.png'),
];

const Icon = ({props, id}) => {
  return props.focused ? (
    <Image key={id} style={styles.iconTab} source={listIconActive[id]} />
  ) : (
    <Image key={id} style={styles.iconTab} source={listIcon[id]} />
  );
};

const ListTabBottom = [
  {
    name: 'Home',
    component: HomeScreen,
  },
  {
    name: 'Cinema',
    component: CinemaScreen,
  },
  {
    name: 'User',
    component: UserScreen,
  },
];

const TabBottom = () => (
  <Tab.Navigator
    screenOptions={{
      headerShown: false,
      tabBarShowLabel: false,
      tabBarActiveTintColor: '#FFF',
      tabBarStyle: styles.tabBottom,
    }}>
    {ListTabBottom.map((item, index) => (
      <Tab.Screen
        options={{tabBarIcon: props => <Icon props={props} id={index} />}}
        name={item.name}
        component={item.component}
      />
    ))}
  </Tab.Navigator>
);
const AppStack = () => {
  const role = useSelector(state => state.slice.role) || 1;
  return (
    <Drawer.Navigator
      drawerContent={props => <CustomDrawer {...props} />}
      screenOptions={{
        headerShown: false,
        drawerStyle: {
          width: 300,
        },
        drawerLabelStyle: {marginLeft: -25},
      }}>
      <Drawer.Screen
        name="Trang chủ"
        component={HomeScreen}
        options={{
          drawerIcon: ({focused, size}) => (
            <Image
              style={{width: 22, height: 22}}
              source={require('../assets/icons/icHome1.png')}
            />
          ),
        }}
      />
      <Drawer.Screen
        name="Đặt vé"
        component={SelectMovieScreen}
        options={{
          drawerIcon: ({focused, size}) => (
            <Image
              style={{width: 22, height: 22}}
              source={require('../assets/icons/icTicket.png')}
            />
          ),
        }}
      />
      <Drawer.Screen
        name="Vé của tôi"
        component={MyTicketScreen}
        options={{
          drawerIcon: ({focused, size}) => (
            <Image
              style={{width: 22, height: 22}}
              source={require('../assets/icons/icMyTicket.png')}
            />
          ),
        }}
      />
      <Drawer.Screen
        name="Thông báo"
        component={MovieShowTimeScreen}
        options={{
          drawerIcon: ({focused, size}) => (
            <Image
              style={{width: 22, height: 22}}
              source={require('../assets/icons/icNotifi.png')}
            />
          ),
        }}
      />
      {/* <Drawer.Screen
        name="Tin mới & ưu đãi"
        component={ManaRoom}
        options={{
          drawerIcon: ({focused, size}) => (
            <Image
              style={{width: 22, height: 22}}
              source={require('../assets/icons/icGift.png')}
            />
          ),
        }}
      /> */}
      {role === 1 ? (
        <Drawer.Screen
          name="Danh sách các phòng"
          component={ListRoomScreen}
          options={{
            drawerIcon: ({focused, size}) => (
              <Image
                style={{width: 22, height: 22}}
                source={require('../assets/icons/icGift.png')}
              />
            ),
          }}
        />
      ) : (
        <></>
      )}
      {role === 1 ? (
        <Drawer.Screen
          name="Quản lý phim"
          component={ManaFilm}
          options={{
            drawerIcon: ({focused, size}) => (
              <Image
                style={{width: 22, height: 22}}
                source={require('../assets/icons/icFilm.png')}
              />
            ),
          }}
        />
      ) : (
        <></>
      )}
    </Drawer.Navigator>
  );
};

function Navigator() {
  const user = useSelector(state => state.slice.user);
  const dispatch = useDispatch();
  const checkAuth = async () => {
    const phone = await AsyncStorage.getItem('@key');
    const pass = await AsyncStorage.getItem('@key2');
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    const raw = JSON.stringify({
      phone: phone,
      password: pass,
    });

    const requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(`${API}/api/login`, requestOptions)
      .then(response => response.json())
      .then(result => {
        if (result?.data?.id) {
          dispatch(setUser(result.data));
        } else {
        }
      })
      .catch(error => {
        console.log('error', error);
      });
  };

  React.useEffect(() => {
    checkAuth();
  }, []);

  return user ? (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {/* <Stack.Screen name="Tab" component={TabBottom} /> */}
        <Stack.Screen name="AppStack" component={AppStack} />
        <Stack.Screen name="User" component={UserScreen} />
        <Stack.Screen name="Info" component={InfoScreen} />
        <Stack.Screen name="Change" component={ChangePassScreen} />
        <Stack.Screen name="SetPass" component={SetPassScreen} />
        <Stack.Screen name="MovieDetails" component={MovieDetailsScreen} />
        <Stack.Screen name="MovieTime" component={MovieShowTimeScreen} />
        <Stack.Screen name="ChooseWater" component={ChooseWaterScreen} />
        <Stack.Screen name="Payment" component={PaymentScreen} />
        <Stack.Screen name="TicketDetails" component={TicketDetailsScreen} />
        <Stack.Screen name="MovieSeat" component={MovieSeatScreen} />
        <Stack.Screen name="First" component={FirstScreen} />
        <Stack.Screen name="ManaRoom" component={ManaRoom} />
      </Stack.Navigator>
    </NavigationContainer>
  ) : (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="SignIn" component={SignInScreen} />
        <Stack.Screen name="SignUp" component={SignUpScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  tabBottom: {
    position: 'absolute',
    width: '70%',
    height: 60,
    backgroundColor: 'white',
    borderRadius: 30,
    paddingBottom: 0,
    left: '15%',
    bottom: 25,
    borderColor: '#cdd9e5',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,

    elevation: 9,
  },
  iconTab: {
    width: 30,
    height: 30,
  },
});

export default Navigator;
