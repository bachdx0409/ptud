import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {useSelector, useDispatch} from 'react-redux';
import {setUser} from '../../redux/slice';
import AsyncStorage from '@react-native-async-storage/async-storage';

const background = require('../image/img_bg_user.jpg');
const avt = require('../image/img_avt.jpg');

const CustomDrawer = props => {
  const user = useSelector(state => state.slice.user);
  const dispatch = useDispatch();

  const checkout = () => {
    Alert.alert('', 'Bạn có muốn đăng xuất không?', [
      {
        text: 'Huỷ',
      },
      {
        text: 'Đăng xuất',
        onPress: async () => {
          await AsyncStorage.setItem('@key', '');
          await AsyncStorage.setItem('@key2', '');
          dispatch(setUser(undefined));
        },
      },
    ]);
  };

  return (
    <View style={{flex: 1, backgroundColor: 'lightyellow'}}>
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={{backgroundColor: 'lightyellow'}}>
        <ImageBackground
          source={require('../image/img_bg_user.jpg')}
          style={{padding: 20, flexDirection: 'row'}}>
          <Image
            source={{uri: user?.image}}
            style={{width: 80, height: 80, borderRadius: 40, marginBottom: 10}}
          />
          <Text
            style={{
              fontWeight: '600',
              fontSize: 18,
              color: '#FFF',
              alignSelf: 'center',
              marginLeft: 20,
            }}>
            {user?.name}
          </Text>
        </ImageBackground>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <View
        style={{
          padding: 20,
          borderTopWidth: 1,
          borderTopColor: '#000',
        }}>
        <TouchableOpacity
          onPress={checkout}
          style={{paddingVertical: 15, flexDirection: 'row'}}>
          <Image
            source={require('../icons/icLogout.png')}
            style={{width: 22, height: 22, marginRight: 5}}
          />
          <Text>Đăng xuất</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  img_avt: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginBottom: 10,
  },
  userName: {
    fontWeight: '600',
    fontSize: 18,
    color: '#FFF',
  },
  line: {
    padding: 20,
    borderTopWidth: 1,
    borderTopColor: 'gray',
  },
  icLogout: {
    width: 22,
    height: 22,
    marginRight: 5,
  },
});

export default CustomDrawer;
