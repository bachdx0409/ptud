import React from 'react';
import Navigator from './src/navigation/Navigation';
import { store } from './src/redux/store'
import { Provider } from 'react-redux'


const App = () => {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
};

export default App;
